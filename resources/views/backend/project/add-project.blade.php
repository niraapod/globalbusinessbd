@extends('backend.layouts.master')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manage Project</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <section class="col-sm-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3> Add Project
                 <a class="btn btn-success float-right btn-sm" href="{{ route('projects.view')}}"><i class="fa fa-list"></i> Projdect List</a>
                </h3>
              </div><!-- /.card-body -->
              <div class="card-body">
              	<form method="post" action="{{ route('projects.store')}}" id="myForm" enctype="multipart/form-data">
              		@csrf
              		<div class="form-row">
              		
              			<div class="form-group col-md-12">
              				<label for="project_name"> Projdect Name</label>
              				<input type="text" name="project_name" class="form-control" id="project_name">
              			</div>
<br/>
              			<div class="form-group col-md-4">
              				<label for="image_one">Projdect Image 1</label>
              				<input type="file" name="image_one" class="form-control" id="image">
              			</div>
                    
              			<div class="form-group col-md-2">
              				<img id="showImage" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>

                    <div class="form-group col-md-4">
              				<label for="image_two">Projdect Image 2</label>
              				<input type="file" name="image_two" class="form-control" id="images">
              			</div>
                    
              			<div class="form-group col-md-2">
              				<img id="showImages" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>

                    <div class="form-group col-md-4">
              				<label for="imagess">Projdect Image 3</label>
              				<input type="file" name="image_three" class="form-control" id="imagess">
              			</div>
                    
              			<div class="form-group col-md-2">
              				<img id="showImagess" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>

                    <div class="form-group col-md-4">
              				<label for="imagesss">Projdect Image 4</label>
              				<input type="file" name="image_four" class="form-control" id="imagesss">
              			</div>

                    <div class="form-group col-md-2">
              				<img id="showfour" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>

                    <div class="form-group col-md-4">
              				<label for="imagessss">Projdect Image 5</label>
              				<input type="file" name="image_five" class="form-control" id="imagessss">
              			</div>

                    <div class="form-group col-md-2">
              				<img id="five" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>

                    <div class="form-group col-md-4">
              				<label for="imagesssss">Projdect Image 6</label>
              				<input type="file" name="image_six" class="form-control" id="imagesssss">
              			</div>
                    
              			<div class="form-group col-md-2">
              				<img id="six" src="{{ url('/upload/no-image.png')}}" style="width: 150px; height: 120px;border: 1px solid #000">
              			</div>
              			<div class="form-group col-md-6">
              				<input type="submit" value="Submit" class="btn btn-primary">
              			</div>

              		</div>

              	</form>

              </div>
            </div>
           
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">
               </div>
            </div>
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

</div>

 <script type="text/javascript">
$(document).ready(function () {
  $('#myForm').validate({
    rules: {
    	 usertype: {
        required: true,
      },
    	name: {
        required: true,
      },

    
      email: {
        required: true,
        email: true,
      },

      password: {
        required: true,
        minlength: 5
      },
      password2: {
        required: true,
        equalTo: '#password'
    },

    messages: {

    	 usertype: {
        required: "Please Select a Roll",
        
      },

    	name: {
        required: "Please enter a Name",
        
      },
     
      email: {
        required: "Please enter a email address",
        
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
     password2: {
        required: "Please provide a Confirm password",
        equalTo: "Your password must be at least 5 characters long"
      },
      
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection
<footer id="footer" class="footer-area">
            <div class="footer-top pt-60 pb-50">
                <div class="container">
                    <div class="row">
                        <!-- footer-address -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget">

                                <h6 class="footer-titel">Get In Touch</h6>
                                <ul class="footer-address">
                                    <li>
                                    <br> 
                                        <div class="address-icon">
                                            <img src="{{asset('frontend/assets/images/icons/location-2.png')}}" alt="Location Icon" title="Location">
                                        </div>
                                        <div class="address-info">
                                            
                                            <span><strong>Registered Office : </strong> 198-202, Nawabpur Tower, Room no# 311
                                            Nawabpur Road, Nawabpur, Dhaka-1100
                                            Bangladesh.</span>
                                </div>
                                    </li>
                                    
                                        <div class="address-icon">
                                            <img src="{{asset('frontend/assets/images/icons/location-2.png')}}" alt="Location Icon" title="Location">
                                        </div>
                                        <div class="address-info">
                                            <span><strong>Branch Office : </strong> Dewanhat City Corporation College, Dewanhat Overbridge, Chattogram 4000.</span>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget">
                                <h6 class="footer-titel">Get In Touch</h6>
                                <ul class="footer-address">
                                   
                                    <li>
                                    <br>
                                        <div class="address-icon">
                                            <img src="{{asset('frontend/assets/images/icons/phone-3.png')}}" alt="Phone Icon" title="Phone">
                                        </div>
                                        
                                        <div class="address-info">
                                            
                                        <span><strong>Phone : </strong>+88 01916 82 21 06</span>
                                            <span><strong>Phone : </strong> +88 01613 23 90 93</span>
                                            <span><strong>Whatsapp : </strong>+88 01916 82 21 06</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="address-icon">
                                            <img src="{{asset('frontend/assets/images/icons/world.png')}}" alt="World Icon" title="Web">
                                        </div>
                                        <div class="address-info">
                                            <span><strong>E-mail : </strong> sales@globalbusinessbd.com</span>
                                            <span><strong>Web : </strong> <a href="https://globalbusinessbd.com"> https://globalbusinessbd.com</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- footer-latest-news -->
                        <div class="row">
                        <div class="footer-social-links">
                            <h6 class="">Stay Wth Us</h6>
                            <ul class="">
                                <li><a href="https://www.facebook.com/Global.Business.Solution.Bd" target="_blank" title="Global Business Solution"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/globalbusinessbd/" target="_blank" title="Global Business Solution"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="" target="_blank" title="Global Business Solution"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="" target="_blank" title="Global Business Solution"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="" target="_blank" title="Global Business Solution"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="" target="_blank" title="Global Business Solution"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                      
                      
                    </div>
                   
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="copyright text-center">
                                <p>Copyright &copy; 2021 | <a href="https://globalbusinessbd.com"><strong>Global Business Solution</strong></a> | All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<div id="sticky-header" class="header-middle-area  transparent-header hidden-xs">
                <div class="container">
                    <div class="full-width-mega-drop-menu">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sticky-logo">
                                    <a href="{{url('/')}}">
                                        <img src="{{asset('frontend/assets/images/logo/logo-gbs.png')}}" alt="Global Business Solution" title="Global Business Solution">
                                    </a>
                                </div>
                                <nav id="primary-menu">
                                  <ul class="main-menu">
                                    <li><a href="{{url('/')}}" title="Home">Home</a></li>
                                    <li><a href="{{route('globalbusiness')}}" title="About Us">About Us</a></li>
                                    <li><a href="{{route('service')}}" title="Services">Services<i aria-hidden="true"></i></a>
                                      
                                    </li>
                                    <li><a href="{{route('productlist')}}" title="Product List">Product</a></li>
                                    
                                    <li><a href="{{route('project')}}" title="Projects">projects<i ></i></a>
                                     
                                    </li>
                                    <li><a href="" title="Services">Download<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                      <ul class="drop-menu">
                                        <li><a href="{{asset('frontend/assets/gbs_profile_2020.pdf')}}" target="_blank"  title="PU Flooring in Bangladesh">Company Profile</a></li>
                                        <li><a href="{{asset('frontend/assets/ADVANCE_WATERPROOFING &_PROTECTION_SYSTEM.pdf')}}" target="_blank" title="Epoxy Flooring in Bangladesh">Brochure</a></li>
                                        
                                      </ul>
                                    </li>

                                    <li><a href="{{route('registration')}}" title="Quotation">Quotation</a></li>
                                   
                                    <!--  <li><a href="https://www.falconsolutionbd.com/download">Download</a></li> -->
                                    <li><a href="{{route('contact')}}" title="Contact Us">Contact Us</a></li>
                                  </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER AREA END -->

        <!-- MOBILE MENU AREA START -->
        <div class="mobile-menu-area hidden-sm hidden-md hidden-lg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                  <li><a href="{{url('/')}}" title="Home">Home</a></li>
                                  <li><a href="{{route('globalbusiness')}}" title="About Us">About Us</a></li>
                                  <li><a href="{{route('service')}}" title="Services">Services <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                   
                                  </li>
                                  <li><a href="{{route('productlist')}}" title="Product List">Product List</a></li>
                                    <li><a href="{{route('project')}}" title="Projects">projects<i  aria-hidden="true"></i></a>
                                     
                                    </li>

                               
                                  </li>

                                    <li><a href="" title="Services">Download<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                      <ul class="drop-menu">
                                        <li><a href="{{asset('frontend/assets/gbs_profile_2020.pdf')}}" style="target:_blank" title="PU Flooring in Bangladesh">Company Profile</a></li>
                                        <li><a href="{{asset('frontend/assets/gbs_profile_2020.pdf')}}" title="Epoxy Flooring in Bangladesh">Brochure</a></li>
                                        
                                      </ul>
                                    </li>
                                    
                                  <li><a href="{{route('registration')}}" title="Meet Our Teams">Calculator</a></li>
                                
                                  <li><a href="{{route('contact')}}" title="Contact Us">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
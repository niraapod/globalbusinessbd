<!doctype html>
<html class="no-js" lang="zxx">

<!-- Mirrored from www.globalbusinessbd.com/pu-flooring-in-bangladesh by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Dec 2021 16:20:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 

  <meta name="author" content="Global Business Solution">
  
  <!-- <meta name="robots" content="noindex"> -->

  <!-- <link rel="canonical" href="https://www.globalbusinessbd.com/" /> -->

  <meta name="viewport" content="width=device-width, initial-scale=1.0">



  <meta name="msvalidate.01" content="FC42D3A7503555FF8A78D6F1B6F18755" />
  
  <!-- for G Sute -->
  <meta name="google-site-verification" content="" />
  
  <meta name="google-site-verification" content="" />
  
  <meta name="google-site-verification" content="" />

  
    <title>Global Business Solution</title>
  <meta name="description" content="pu flooring in bangladesh, Global Business Solution  provide pu Industrial flooring products contractor, supplier, manufacturer best service with cheapest price.">
  
  <meta name="keywords" content="PU Flooring In Bangladesh, PU Industrial Flooring In Bangladesh, Industrial PU Flooring In Bangladesh">
  
  
  <link rel="icon" href="{{ url('css/gbs.ico') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">



    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/bootstrap.min.css')}}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{asset('frontend/assets/lib/css/nivo-slider.css')}}"/>
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/core.css')}}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/shortcode/shortcodes.css')}}">
    <!-- Theme main style -->

    <script src="{{asset('frontend/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0- 
     alpha/css/bootstrap.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" 
     href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/responsive.css')}}">
    <!-- User style -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/custom.css')}}">
    
    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/style-customizer.css')}}">

    <link rel="stylesheet" href="{{asset('frontend/assets/css/form.css')}}">

    <!-- <link rel="canonical" href="https://www.globalbusinessbd.com/" /> -->

    <!-- Modernizr JS -->
   

   <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147245144-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-147245144-1');
    </script>
    
  
  
</head>

<body>

    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = '../connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Your Chat Plugin code -->
        <div class="fb-customerchat"
          attribution=install_email
          page_id="187681425329211">
        </div>
        <!-- Load Facebook SDK END -->
  
        <!-- HEADER AREA START -->
        <header class="header-area header-wrapper">
            <div class="header-top-bar bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <div class="logo">
                                <a href="{{url('/')}}">
                                    <img src="{{asset('frontend/assets/images/logo/logo-gbs.png')}}" alt="Global Business Ltd" title="Global Business Ltd">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7 hidden-sm hidden-xs">
                            <div class="company-info clearfix">
                                <div class="company-info-item">
                                    <div class="header-icon">
                                        <img src="{{asset('frontend/assets/images/icons/phone.png')}}" alt="Phone" title="Phone">
                                    </div>
                                    <div class="header-info">
                                        <h6>+88 01916 82 21 06</h6>
                                        <P>We are open 9 am - 6pm</P>
                                    </div>
                                </div>
                                <div class="company-info-item">
                                    <div class="header-icon">
                                        <img src="{{asset('frontend/assets/images/icons/quotation.png')}}" alt="E-mail" title="E-mail">
                                
                                    </div>

                                    <div class="header-info">
                                        <h6><a href="{{route('registration')}}" title="">Quotation</a></h6>
                                        <P>Get You Quote</P>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        @include('frontend.layouts.menubar')

<!-- start content -->
@yield('content')
<!-- end content -->
@include('frontend.layouts.footer')
</div>
 
    <script src="{{asset('frontend/assets/js/vendor/jquery-3.1.1.min.js')}}"></script>
    <!-- Bootstrap framework js -->
    <script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>
    <!-- Nivo slider js -->    
    <script src="{{asset('frontend/assets/lib/js/jquery.nivo.slider.js')}}"></script>
    <!-- ajax-mail js -->
    <script src="{{asset('frontend/assets/js/ajax-mail.js')}}"></script>
    <!-- All js plugins included in this file. -->
    <script src="{{asset('frontend/assets/js/plugins.js')}}"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="{{asset('frontend/assets/js/main.js')}}"></script>


    <!-- Gallery fancyBox Start-->
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="{{asset('frontend/assets/fancybox/source/jquery.fancybox3899.css')}}"?v=2.1.6 type="text/css" media="screen" />
    <script type="text/javascript" src="{{asset('frontend/assets/fancybox/source/jquery.fancybox.pack3899.js')}}"?v=2.1.6></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>

    <script src="{{ asset('/js/custom.js')}}"></script>
</body>


<!-- Mirrored from www.globalbusinessbd.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Dec 2021 16:19:52 GMT -->
</html>
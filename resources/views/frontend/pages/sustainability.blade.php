@extends('frontend.layouts.master');
@section('content')

<div class="content post-9618 page type-page status-publish hentry">
<div class="nosidebar">

	<div class="blank-reveal-area"></div>

							<div class="post-article">
<div id="sustain" class="box-container vc_row wpb_row vc_row-fluid  mainHeaderText topImageSection vc_custom_1540537266882"><div><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-9">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
    <div class="wpb_single_image wpb_content_element vc_align_left">
        <div class="wpb_wrapper">
            
            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="922" height="250" src="../../wp-content/uploads/2018/07/ABOUT-US_Sustainability.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://www.ardexendura.com/wp-content/uploads/2018/07/ABOUT-US_Sustainability.jpg 922w, https://www.ardexendura.com/wp-content/uploads/2018/07/ABOUT-US_Sustainability-600x163.jpg 600w, https://www.ardexendura.com/wp-content/uploads/2018/07/ABOUT-US_Sustainability-300x81.jpg 300w, https://www.ardexendura.com/wp-content/uploads/2018/07/ABOUT-US_Sustainability-768x208.jpg 768w" sizes="(max-width: 922px) 100vw, 922px" /></div>
        </div>
    </div>

			</div>
		</div>
	</div>
	<div class="wpb_column vc_column_container vc_col-sm-3">
		<div class="vc_column-inner vc_custom_1530773320864">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2>SUSTAINABILITY</h2>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us vc_custom_1532076387520 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>THE FACTS SPEAK FOR THEMSELVES</strong></h3>
<ul>
<li>High &#8211; coverage Global Business Solution products let you take care of large surface areas with a minimum of material, which cuts down on processing, transport and waste packaging.</li>
<li>Ultra &#8211; fast, easy-to- use Global Business Solution products allow you to complete projects in as little as one day &#8211; which protects the environment by reducing trips back and forth to the building site.</li>
<li>Global Business Solution powder products are mixed with water right on the construction site, reducing costs and time needed for transport.</li>
<li>High &#8211; quality Global Business Solution sub-floor smoothing compounds deliver an extremely even surface, which means far less adhesive is required to affix the flooring.</li>
<li>The extraordinarily long life of all Global Business Solution products helps save resources.</li>
<li>Global Business Solution products designed for renovating existing buildings help reduce the strain on the environment, as refurbishment consumes far fewer resources than new builds.</li>
<li>Made from carefully selected raw materials, virtually all Global Business Solution products are 100% recyclable, which saves natural resources.</li>
<li>By continually investing in state-of- the-art production technology for its plants, Global Business Solution achieves significant energy savings.</li>
</ul>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us_even vc_custom_detheme3 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>SHAPING THE FUTURE</strong></h3>
<p>Sustainability is one of the defining issues of our time. Climate change and the financial crisis have driven home the devastating consequences of action geared to short-term goals. In contrast, acting sustainably means shaping the future responsibly over the long term. It calls for the sparing use of economic resources, preserving their value, coupled with protecting our environment – all in a spirit of cooperation based on fairness and respect. Long before it became a buzzword, the concept of sustainability was already an integral part of the Global Business Solution corporate philosophy. It is the common thread that guides the way our family business thinks and acts.</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us vc_custom_1540030352647 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>OPERATING COST-EFFICIENTLY FOR PLANETS&#8217;S SAKE</strong></h3>
<p>At Global Business Solution, we see ecology and economy as two sides of the same coin. Climate change demands that valuable resources are used sparingly. Global Business Solution sets the bar high with products that are not only easy to work but also go a long way. It’s our answer to preserving the planet for the future – and providing value for money. In addition, Global Business Solution offers emission-free, consumer- and user-friendly products for almost all applications. Sustainability is also a principle that informs business policy at Global Business Solution. Our latest investments in production facilities are strengthening local business in our sales markets, safeguarding jobs wherever we aim to sell our products, boosting the region’s economy and at the same time easing the load on the environment thanks to short transport distances.</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us_even vc_custom_detheme5 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>POINTING THE WAY IN BUILDING CHEMICALS</strong></h3>
<p>Global Business Solution has been a quality and innovation leader in its industry. Our cutting-edge production methods translate into premium quality and functional performance coupled with excellent reliability and durability. With Global Business Solution products, you know you’ve made the right choice when it comes to sustainable construction. To permanently safeguard the quality of our products, all raw materials are subject to rigorous testing. Regular quality analyses are conducted throughout the manufacturing process right up to the finished product. Work on new developments, especially with a view to enhancing environmental performance, is continually underway at Global Business Solution. Global Business Solution has invested millions to ensure that innovation continues apace – and will be spending even more in years to come.</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us vc_custom_1540030479511 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>ACTING IN PARTNERSHIP</strong></h3>
<p>Unity is strength. That’s why it’s so important to forge dependable partnerships – and make sure they go the distance by cultivating a climate of transparency, confidence and unshakable trust. At Global Business Solution, the principle of sustainable partnerships is one of the cornerstones of our corporate philosophy – both within the company and beyond. This is why the yardstick for our collaboration with market partners is lasting mutual benefit. We engage in ongoing dialogue to ensure that we incorporate our partners’ needs and expectations into all our business decisions. Naturally, as a family business, our in-house partners are also especially close to our heart. Our staff is the foundation that underpins our success. We are committed to investing in them so that they will continue to feel at home at Global Business Solution for many years to come.</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div><div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us_even vc_custom_detheme7 not-flex"><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3><strong>SUSTAINABLE CONSTRUCTION</strong></h3>
<p>Sustainable construction is gathering momentum. The aim is to design buildings that will contribute to the long-term conservation of energy and resources as well as the health and happiness of occupants. For some time now, various building certification systems have been available to evaluate and honour sustainability. These systems also take into account different types of building, for instance, office and administrative blocks, schools,airports, etc. Such ratings always consider the sustainability of a building as a whole, so individual building materials cannot be categorised as sustainable or unsustainable. Aside from the purely environmental aspect, the make-or- break question is whether the functionality and quality of the materials used will positively impact the building’s sustainability over its entire life cycle.</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div>
						</div>
			</div>
	</div>

@endsection
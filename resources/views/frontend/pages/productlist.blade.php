@extends('frontend.layouts.master');
@section('content')
 <!-- BREADCRUMBS AREA START -->
 <div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">Product List</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="index.html">Home</a></li>
                                <li>Product List</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- OUR AGENTS AREA START -->
            <div class="our-agents-area  pt-115 pb-60">
                <div class="container">
                    <div class="our-agents">
                   
                        <div class="row">
                          
                            
                        <div class="section-title-3 text-center">
                                <h2>Waterproofing</h2>
</div>
@foreach($alldata as $product)
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                        <ul class="flat-desc">
                                            <li>
                                            <span>{{$product->product_name}}
                                           </span>
                                            </li>
                                        </ul>
                                            <img src="{{(!empty($product->image))?url('/upload/product_images/'.$product->image):url('/upload/no-image.png')}}" alt="Global Business Solution">
                                            <div class="flat-link">
                                            <a href="{{(!empty($product->technical_datasheet))?url('/upload/technical_datasheet/'.$product->technical_datasheet):url('/upload/no-datasheet.pdf')}}" title="Construction Chemicals in Bangladesh">Technical Data Sheet</a>
                                            <br>
                                            <br>
                                            <a href="{{(!empty($product->test_report))?url('/upload/testreport/'.$product->test_report):url('/upload/no-testreport.pdf')}}" title="Construction Chemicals in Bangladesh">Test report
</a>
                                        </div>
											
                                        </div>
                                       
                                       
                                    </div>
                                </div>

@endforeach
                        


</div> 

                <div class="container">
                    <div class="our-agents">
                                <div class="row">
                                <div class="section-title-3 text-center">
                                <h2>Thermal Insulation & Headproofing
</h2>
                              
                            </div>
@foreach($alldataa as $product)
                                <!-- single-agent -->
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                        <ul class="flat-desc">
                                            <li>
                                                <span>{{$product->product_name}}

                                           </span>
                                            </li>
                                        </ul>
                                        <img src="{{(!empty($product->image))?url('/upload/products_images/'.$product->image):url('/upload/no-image.png')}}" alt="Global Business Solution">
                                            <div class="flat-link">
                                            <a href="{{(!empty($product->technical_datasheet))?url('/upload/technicals_datasheet/'.$product->technical_datasheet):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Technical Data Sheet</a>
                                            <br>
                                            <br>
                                            <a href="{{(!empty($product->test_report))?url('/upload/testreports/'.$product->test_report):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Test report
</a>
                                        </div>
											
                                        </div>
                                       
                                       
                                    </div>
                               
                                    </div>      
				@endforeach				
                </div> 
                <div class="container">
                    <div class="our-agents">
                                <div class="row">
                                <div class="section-title-3 text-center">
                                <h2>   <h2>Repair & Bonding Agent
</h2>
</h2>
                              
                            </div>
                            @foreach($alldataaa as $product)
                                <!-- single-agent -->
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                        <ul class="flat-desc">
                                            <li>
                                                <span>{{$product->product_name}}

                                           </span>
                                            </li>
                                        </ul>
                                        <img src="{{(!empty($product->image))?url('/upload//productss_images/'.$product->image):url('/upload/no-image.png')}}" alt="Global Business Solution">
                                            <div class="flat-link">
                                            <a href="{{(!empty($product->technical_datasheet))?url('/upload/technicalss_datasheet/'.$product->technical_datasheet):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Technical Data Sheet</a>
                                            <br>
                                            <br>
                                            <a href="{{(!empty($product->test_report))?url('/upload/testreportss/'.$product->test_report):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Test report
</a>
                                        </div>
											
                                        </div>
                                       
                                       
                                    </div>
                               
                                    </div>      
				@endforeach				
                </div> </div>

                <div class="container">
                    <div class="our-agents">
                                <div class="row">
                                <div class="section-title-3 text-center">
                                <h2>Flooring Protection
</h2>
                              
                            </div>
                            @foreach($flooring as $product)
                                <!-- single-agent -->
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="flat-item">
                                        <div class="flat-item-image">
                                        <ul class="flat-desc">
                                            <li>
                                                <span>{{$product->product_name}}

                                           </span>
                                            </li>
                                        </ul>
                                        <img src="{{(!empty($product->image))?url('/upload/productsss_images/'.$product->image):url('/upload/no-image.png')}}" alt="Global Business Solution">
                                            <div class="flat-link">
                                            <a href="{{(!empty($product->technical_datasheet))?url('/upload/technicalsss_datasheet/'.$product->technical_datasheet):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Technical Data Sheet</a>
                                            <br>
                                            <br>
                                            <a href="{{(!empty($product->test_report))?url('/upload/testreportsss/'.$product->test_report):url('/upload/no-image.png')}}" title="Construction Chemicals in Bangladesh">Test report
</a>
                                        </div>
											
                                        </div>
                                       
                                       
                                    </div>
                               
                                    </div>      
				@endforeach				
                </div> </div>
               
								
                              
                               
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>  
@endsection
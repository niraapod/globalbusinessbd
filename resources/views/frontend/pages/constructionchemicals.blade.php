@extends('frontend.layouts.master')
@section('content')
   <!-- BREADCRUMBS AREA START -->
   <div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">Construction Chemicals</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="index.html">Home</a></li>
                                <li>Construction Chemicals</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- SERVICES AREA START -->
            <div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                    <div class="featured-flat">
                        <div class="row">
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="grouting-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Grouting"></a>
                                        <div class="flat-link">
                                            <a href="grouting-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Grouting</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="repair-mortar-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Repair Mortar"></a>
                                        <div class="flat-link">
                                            <a href="repair-mortar-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Repair Mortar</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="concrete-admixtures-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Concrete Admixtures"></a>
                                        <div class="flat-link">
                                            <a href="concrete-admixtures-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Concrete Admixtures</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="joint-sealants-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Joint Sealants"></a>
                                        <div class="flat-link">
                                            <a href="joint-sealants-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Joint Sealants</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="crack-repair-injection-systems-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Crack Repair Injection Systems"></a>
                                        <div class="flat-link">
                                            <a href="crack-repair-injection-systems-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Crack Repair Injection Systems</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="bonding-agents-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Bonding Agents"></a>
                                        <div class="flat-link">
                                            <a href="bonding-agents-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Bonding Agents</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="concrete-auxiliaries-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/Construction-Chemicals.jpg')}}" alt="Concrete Auxiliaries"></a>
                                        <div class="flat-link">
                                            <a href="concrete-auxiliaries-in-bangladesh.html">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                            <li>
                                                <span>Concrete Auxiliaries</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                        </div>
                    </div>
                </div>
            </div>
@endsection
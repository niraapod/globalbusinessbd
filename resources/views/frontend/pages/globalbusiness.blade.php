@extends('frontend.layouts.master');
@section('content')
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/assets/images/icons/gbs.png')}}">
    <!-- BREADCRUMBS AREA START -->
	<div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">About Us</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>About Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area ptb-115">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6 col-xs-12">
                            <div class="section-title mb-30"> 
                            <h4> Global Business Solution</h4>
                            </div>
                            <div class="about-sheltek-info">
                                <p class="text-justify">
                                <b>Our mission</b> is to supply quality building chemicals, developed to meet latest European Union standards to our valuable Bangladeshi Clients. Our commitment to Bangladesh construction Industry is to identify and bring most tested and proven waterproofing and Insulation products. 
                                </p>

                                <p class="text-justify">
                                <b>Our vision</b> is to become a prominent building chemical company in Bangladesh through continuous efforts to understand local requirements and to share our International experience.                                 </p>

                                <p class="text-justify">
                                We supply bulk quantity of waterproofing product to Government Origination - Rajuk, PWD, LGED, NHA Including mega project- Rampal power plant, Matarbari power plant, Paira Deep sea, Padma Rail way, all project approved our imported product, 
At the core of our values is: 
We value open and honest communication both internally, with our employees, externally, with clients and partners. 
We are a strong company because of our collective knowledge of the industry requirements. 
International experience gives us a foundation for discernment and the ability to carefully select products, application methods to suit client’s expectations. 
We strongly believe the input from employees and end users plays vital role to upgrade product ranges. 
By sustaining the Vision and supporting the Mission, GBS will achieve prominence as an most reliable company in building chemical industry in Bangladesh. 
GBS will hold stock of Imported Bituminous Membrane, Acrylic, PU coatings, cementations waterproofing, PU & epoxy grouting - manufactured from KSA, KOREA, GREECE . To start with, we are holding enough stock of following most established and time tested all Products in Bangladesh.
                                </p>
                                  
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6 col-xs-12">
                            <div class="about-image">
                                <img src="{{asset('frontend/assets/images/about/web-page--image-01.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>

        
   
             
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4 style="" >   THE FACTS SPEAK FOR THEMSELVES</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                              
High – coverage Global Business Solution products let you take care of large surface areas with a minimum of material, which cuts down on processing, transport and waste packaging.
Ultra – fast, easy-to- use Global Business Solution products allow you to complete projects in as little as one day – which protects the environment by reducing trips back and forth to the building site.
Global Business Solution powder products are mixed with water right on the construction site, reducing costs and time needed for transport.
High – quality Global Business Solution sub-floor smoothing compounds deliver an extremely even surface, which means far less adhesive is required to affix the flooring.
The extraordinarily long life of all Global Business Solution products helps save resources.
Global Business Solution products designed for renovating existing buildings help reduce the strain on the environment, as refurbishment consumes far fewer resources than new builds.
Made from carefully selected raw materials, virtually all Global Business Solution products are 100% recyclable, which saves natural resources.
By continually investing in state-of- the-art production technology for its plants, Global Business Solution achieves significant energy savings.
                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>

        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4 style="" >   SHAPING THE FUTURE
</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                Sustainability is one of the defining issues of our time. Climate change and the financial crisis have driven home the devastating consequences of action geared to short-term goals. In contrast, acting sustainably means shaping the future responsibly over the long term. It calls for the sparing use of economic resources, preserving their value, coupled with protecting our environment – all in a spirit of cooperation based on fairness and respect. Long before it became a buzzword, the concept of sustainability was already an integral part of the Global Business Solution corporate philosophy. It is the common thread that guides the way our family business thinks and acts.


                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>

            <!-- SUBSCRIBE AREA END -->
            <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4  >   OPERATING COST-EFFICIENTLY FOR PLANETS’S SAKE

</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                At Global Business Solution, we see ecology and economy as two sides of the same coin. Climate change demands that valuable resources are used sparingly. Global Business Solution sets the bar high with products that are not only easy to work but also go a long way. It’s our answer to preserving the planet for the future – and providing value for money. In addition, Global Business Solution offers emission-free, consumer- and user-friendly products for almost all applications. Sustainability is also a principle that informs business policy at Global Business Solution. Our latest investments in production facilities are strengthening local business in our sales markets, safeguarding jobs wherever we aim to sell our products, boosting the region’s economy and at the same time easing the load on the environment thanks to short transport distances.



                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4  >   POINTING THE WAY IN BUILDING CHEMICALS


</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                Global Business Solution has been a quality and innovation leader in its industry. Our cutting-edge production methods translate into premium quality and functional performance coupled with excellent reliability and durability. With Global Business Solution products, you know you’ve made the right choice when it comes to sustainable construction. To permanently safeguard the quality of our products, all raw materials are subject to rigorous testing. Regular quality analyses are conducted throughout the manufacturing process right up to the finished product. Work on new developments, especially with a view to enhancing environmental performance, is continually underway at Global Business Solution. Global Business Solution has invested millions to ensure that innovation continues apace – and will be spending even more in years to come.




                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4  >   ACTING IN PARTNERSHIP



</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                Unity is strength. That’s why it’s so important to forge dependable partnerships – and make sure they go the distance by cultivating a climate of transparency, confidence and unshakable trust. At Global Business Solution, the principle of sustainable partnerships is one of the cornerstones of our corporate philosophy – both within the company and beyond. This is why the yardstick for our collaboration with market partners is lasting mutual benefit. We engage in ongoing dialogue to ensure that we incorporate our partners’ needs and expectations into all our business decisions. Naturally, as a family business, our in-house partners are also especially close to our heart. Our staff is the foundation that underpins our success. We are committed to investing in them so that they will continue to feel at home at Global Business Solution for many years to come.





                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>

        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4  >   SUSTAINABLE CONSTRUCTION



</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                Sustainable construction is gathering momentum. The aim is to design buildings that will contribute to the long-term conservation of energy and resources as well as the health and happiness of occupants. For some time now, various building certification systems have been available to evaluate and honour sustainability. These systems also take into account different types of building, for instance, office and administrative blocks, schools,airports, etc. Such ratings always consider the sustainability of a building as a whole, so individual building materials cannot be categorised as sustainable or unsustainable. Aside from the purely environmental aspect, the make-or- break question is whether the functionality and quality of the materials used will positively impact the building’s sustainability over its entire life cycle.






                                </p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="section-title mb-30 "> 
                            <h4  >  WHY GLOBAL BUSINESS SOLUTION ?




</h4>
                            </div>
                            <div class="about-sheltek-info">
                                
                            

                                <p class="text-justify">
                                Global Business Solution is synonymous with innovative production systems and the highest standards of customer service. Our success is driven first and foremost by people, our employees, who deliver on our brand promise each and every day. As an employer, we embrace a culture rooted in core corporate values. Our principal focus is to maintain and increase our capacity for innovation. We believe this can only be achieved in a climate of utmost transparency, open interaction, thought and action in a spirit of partnership as well as mutual respect based on trust.

As a globally expanding business, we offer multifaceted opportunities for career development. This naturally applies to all those who are already part of the Global Business Solution family. At Global Business Solution, careers don’t end at departmental boundaries or national borders. For highly motivated colleagues from all departments, the path is open to develop their careers and attain challenging, responsible positions at Global Business Solutioncompanies across the globe. We pursue this approach at all levels of the company, right up to our highest management tier, the Global Management Committee.

However, as a fast-growing organisation aspiring to top-rung technical leadership in the markets we serve, we cannot fill all positions with internal resources despite intensive personnel development efforts. This is why we are always interested in getting to know outstanding candidates (male and female) from all relevant disciplines and backgrounds to fill vacancies within the Global Business Solution Group.





                                </p>

                                <p class="text-justify">
                              <b> WHY CHOOSE US ?</b><br>


GREAT ENVIRONMENT
As a globally expanding business, we offer multifaceted opportunities for career development


FRIENDLY WORKING CULTURE
As an employer, we embrace a culture rooted in core corporate values.


EXCITING PRIZES ON TARGETS
Our principal focus is to maintain and increase our capacity for innovation.
                                </p>

                                <p class="text-justify">
                                Come join our team

If interested to join our company, please contact our HR department and send your updated resume on careers@globalbusinessbd.com

</p>
                                  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>

            <!-- PRODICTS BRAND AREA START -->
            <div class="blog-area pb-80  pt-95">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h4>We import and purchase products from this country....</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="blog-">
                            <!-- blog-item -->
                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/saudiarabia.jpg')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>SAUDI ARABIA</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->
                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/GREECE.jpg')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>GREECE</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->
                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/KOREA.png')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>KOREA</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- blog-item -->


                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/china.jpg')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>CHINA</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/India.jpg')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>INDIA</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <div class="col-md-2">
                                <article class="blog-item bg-gray">
                                    <div class="blog-image">
                                        <img src="{{asset('frontend/assets/images/products/Egypt.png')}}" alt="Global Business Solution">
                                    </div>
                                    <div class="blog-info">
                                        <div class="post-title-time">
                                            <h5>EGYPT</h5>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <img src="{{asset('frontend/assets/images/DistributorApplicator/DistributorApplicator.png')}}"" alt="Global Business Solution">
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- BLOG AREA END -->
        </section>
        <!-- End page content -->
@endsection
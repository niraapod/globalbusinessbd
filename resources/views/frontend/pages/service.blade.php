@extends('frontend.layouts.master');
@section('content')
<div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title-2 text-center">
                                <h2>OUR SERVICES</h2>
                                <p>Service is important to you, so it is important to us. Our business success is depends on best service. Our goal is to create a customer to us dependable and reliable. We are achieving this by providing continue support and prompt service after the sale. Be assured that you are buying World best product and application service.</p>
                            </div>
                        </div>
                    </div>
                  
                   
<div class="featured-flat">
                        <h2 class="section-title-2 text-center">Industrial & Commercial Flooring</h2>
                        <div class="row">
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('IndustrialEpoxy')}}">More Details</a>
                                        </div>

                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Industrial Epoxy Flooring Solution


</span>
                                            </li>
                                            </ul>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('pucoating')}}"><img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('pucoating')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU Floor Coating



</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('puself')}}"><img src="{{asset('frontend/assets/images/flat/pusl.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('puself')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU Self Level Flooring




</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('puconcreate')}}"><img src="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('puconcreate')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU concrete Flooring





</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epu')}}"><img src="{{asset('frontend/assets/images/flat/epu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epu')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                EPU Flooring






</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('antistaticepoxy')}}"><img src="{{asset('frontend/assets/images/flat/esdantistatic.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('antistaticepoxy')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Anti Static Epoxy Flooring






</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('EpoxyHybrid')}}"><img src="{{asset('frontend/assets/images/flat/epoxy.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('EpoxyHybrid')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Epoxy Hybrid Flooring Coating







</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('ChemicalResistance')}}"><img src="{{asset('frontend/assets/images/flat/chemical.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('ChemicalResistance')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Chemical Resistance Epoxy Floor








</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('polishedconcretee')}}"><img src="{{asset('frontend/assets/images/flat/concrete.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('polishedconcretee')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Polished Concrete Epoxy Floor


</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epoxycementitiousflooring')}}"><img src="{{asset('frontend/assets/images/flat/cementitiousflooring.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epoxycementitiousflooring')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Epoxy Cementitious Flooring


</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>
                           
                            </div>

                            
                          


                            <div class="section-title-3 text-center">
                            <h2 >Concrete Waterproofing</h2>

                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('vinylflooring')}}"><img src="{{asset('frontend/assets/images/flat/11.png')}}" alt="Vinyl Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('vinylflooring')}}">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                           
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('selflevelingepoxy')}}"><img src="{{asset('frontend/assets/images/flat/12.png')}}" alt="Self Leveling Epoxy Flooring"></a>
                                        <div class="flat-link">
                                            <a href="{{route('selflevelingepoxy')}}">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('metallicepoxyflooring')}}"><img src="{{asset('frontend/assets/images/flat/13.png')}}" alt="Metallic Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('metallicepoxyflooring')}}">More Details</a>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epoxyparkingflooring')}}"><img src="{{asset('frontend/assets/images/flat/15.png')}}" alt="Epoxy Parking Solution"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epoxyparkingflooring')}}">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="epoxy-3d-flooring-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/16.png')}}" alt="3D Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="epoxy-3d-flooring-in-bangladesh.html">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="epoxy-wall-coating-and-paint-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/17.png')}}" alt="Epoxy Wall Coating and Paint"></a>
                                        <div class="flat-link">
                                            <a href="epoxy-wall-coating-and-paint-in-bangladesh.html">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                           
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="floor-hardener-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/18.png')}}" alt="Floor Hardener Solutions"></a>
                                        <div class="flat-link">
                                            <a href="floor-hardener-in-bangladesh.html">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>
                           

                   
<div class="featured-flat">
                        <h2 class="section-title-2 text-center">PAINT & COATING</h2>
                       

              
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('fairfaceplaster')}}"><img src="{{asset('frontend/assets/images/flat/20.png')}}" alt="Fair Face Plaster"></a>
                                        <div class="flat-link">
                                            <a href="{{route('fairfaceplaster')}}">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('waterproofing')}}"><img src="{{asset('frontend/assets/images/flat/21.png')}}" alt="Waterproofing Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('waterproofing')}}">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/22.png')}}" alt="Damp Proofing Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            </div>


                            </div>
                                </div>
                            
                            

                <div class="container">
                   
<div class="featured-flat">
<h2 class="section-title-2 text-center">Repearing</h2>

                           
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/23.png')}}" alt="Construction Chemicals"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/24.png')}}" alt="PU Foam Spray"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/25.png')}}" alt="PU Foam Spray"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/26.png')}}" alt="Commercial & Residential Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/27.png')}}" alt="Commercial & Residential Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                          

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/28.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/29.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/30.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                           
                            </div>

                            </div>
                           

                           
                <div class="container">
                    
                    <div class="featured-flat">
                        <h2 class="section-title-2 text-center">Heat Proofing</h2>
                        <div class="row">
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('puflooring')}}"><img src="{{asset('frontend/assets/images/flat/31.png')}}" alt="PU Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('puflooring')}}">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/33.png')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                           
                 
                          
                            </div>
                            </div>   </div>
                            </div>
                            </div>
                            </div>
@endsection
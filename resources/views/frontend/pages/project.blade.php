@extends('frontend.layouts.master');
@section('content') 

        <!-- BREADCRUMBS AREA START -->
        <div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h2 class="breadcrumbs-title">Our Projects</h2>
                            <ul class="breadcrumbs-list">
                                <li><a href="index.html">Home</a></li>
                                <li>Our Projects</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">
            
            <!-- ABOUT SHELTEK AREA START -->
            <div class="about-sheltek-area ptb-115">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Gallery fancyBox Start -->

@foreach($alldata as $project)
                            <div style="text-align: center; color:red;" class="">
                                <h2>{{$project->project_name}}
</h2>
                              
                            </div>
                            <div> 
                            <a class="fancybox" rel="group" href="{{(!empty($project->image_one))?url('/upload/project_images/'.$project->image_one):url('/upload/no-image.png')}}"><img src="{{(!empty($project->image_one))?url('/upload/project_images/'.$project->image_one):url('/upload/no-image.png')}}" alt="" /></a>
                            <a class="fancybox" rel="group" href="{{(!empty($project->image_two))?url('/upload/image_two/'.$project->image_two):url('/upload/no-image.png')}}"><img src="{{(!empty($project->image_two))?url('/upload/image_two/'.$project->image_two):url('/upload/no-image.png')}}" alt="" /></a>
                             <a class="fancybox" rel="group" href="{{(!empty($project->image_three))?url('/upload/image_three/'.$project->image_three):url('/upload/no-image.png')}}"><img src="{{(!empty($project->image_three))?url('/upload/image_three/'.$project->image_three):url('/upload/no-image.png')}}" alt="" /></a>
                             <a class="fancybox" rel="group" href="{{(!empty($project->image_four))?url('/upload/image_four/'.$project->image_four):url('/upload/no-image.png')}}"><img src="{{(!empty($project->image_four))?url('/upload/image_four/'.$project->image_four):url('/upload/no-image.png')}}" alt="" /></a>
                             <a class="fancybox" rel="group" href="{{(!empty($project->image_five))?url('/upload/image_five/'.$project->image_five):url('/upload/no-image.png')}}"><img src="{{(!empty($project->image_five))?url('/upload/image_five/'.$project->image_five):url('/upload/no-image.png')}}" alt="" /></a>
                          


             @endforeach              

                            
                         
                           
                            <!-- Gallery fancyBox End -->
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
            <!-- ABOUT SHELTEK AREA END -->
            <!-- SERVICES AREA END -->
        </section>
        
        
@endsection
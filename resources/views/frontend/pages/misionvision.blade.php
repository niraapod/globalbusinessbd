@extends('frontend.layouts.master');
@section('content')
 																					<!-- start content -->
<div class="content post-9403 page type-page status-publish hentry">
<div class="nosidebar">

	<div class="blank-reveal-area"></div>

							<div class="post-article">
<div class="box-container vc_row wpb_row vc_row-fluid  topImageSection vc_custom_1540536929051"><div><div class="inner-flex">
	<div class="wpb_column vc_column_container vc_col-sm-9">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
    <div class="wpb_single_image wpb_content_element vc_align_left">
        <div class="wpb_wrapper">
            
            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="922" height="250" src="{{asset('frontend/wp-content/uploads/2018/07/ABOUT-US_Vision-Mission.jpg')}}" class="vc_single_image-img attachment-full" alt=""/></div>
        </div>
    </div>

			</div>
		</div>
	</div>
	<div class="mainHeaderText wpb_column vc_column_container vc_col-sm-3">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>We are</p>
<h2>COMMITTED</h2>
<p>to meet the needs of our customers by providing high-performance products and solutions to the construction industry</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div>


<div class="box-container"><div class="vc_row wpb_row vc_row-fluid  about_us vc_custom_1533200967097 not-flex"><div class="inner-flex">
	<div class="ardVission wpb_column vc_column_container vc_col-sm-6">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p style="text-align: justify; font-family: 'Lato', sans-serif; line-height: 1.8; font-size: 20px;"><strong>VISION</strong></p>

		</div> 
	</div> 
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><img src="../../Global Business Solution/images/mission.png" alt="" width="14%" /></p>
<p style="text-align: justify; font-family: 'Lato', sans-serif; line-height: 1.8;">“The vision of the Global Business Solution is to be one of the world’s leading solution providers of high-performance specialty building materials”,  CEO Global Business Solution and CEO of Global Business Solution Bangladesh.&#8221;</p>

		</div> 
	</div> 
			</div>
		</div>
	</div>
	<div class="ardMission wpb_column vc_column_container vc_col-sm-6">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p style="text-align: justify; font-family: 'Lato', sans-serif; line-height: 1.8; font-size: 20px;"><strong>MISSION</strong></p>

		</div> 
	</div> 
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><img src="../../Global Business Solution/images/vision.png" alt="" width="14%" /></p>
<p style="text-align: justify; font-family: 'Lato', sans-serif; line-height: 1.8;">&#8220;Global Business Solution  is committed to meet the needs of its customers by providing high-performance products and solutions to the construction industry in an environment friendly manner, through a highly competent, inspired and motivated team.&#8221;</p>

		</div> 
	</div> 
			</div>
		</div>
	</div></div></div></div>
						</div>
			</div>
	</div>
@endsection
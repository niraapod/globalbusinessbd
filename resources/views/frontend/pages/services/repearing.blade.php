@extends('frontend.layouts.master');
@section('content')


<div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                   
<div class="featured-flat">
<h2 class="section-title-2 text-center">Repearing</h2>

                           
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/23.png')}}" alt="Construction Chemicals"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/24.png')}}" alt="PU Foam Spray"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/25.png')}}" alt="PU Foam Spray"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/26.png')}}" alt="Commercial & Residential Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/27.png')}}" alt="Commercial & Residential Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                          

                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/28.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/29.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href=""><img src="{{asset('frontend/assets/images/flat/30.png')}}" alt="Expansion Joint Work"></a>
                                        <div class="flat-link">
                                            <a href="">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                           
                            </div>

                            </div>
                            </div>
                           
                            </div>
                            @endsection
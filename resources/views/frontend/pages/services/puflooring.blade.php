@extends('frontend.layouts.master');
@section('content')


<div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">PU Flooring In Bangladesh</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>PU Flooring Solution</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- Start page content -->
 <section id="page-content" class="page-wrapper">

<!-- PROPERTIES DETAILS AREA START -->
<div class="properties-details-area pt-115 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- pro-details-image -->
                <div class="pro-details-image mb-60">
                    <div class="pro-details-big-image">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="pro-1">
                                <a href="{{asset('frontend/assets/images/flat/pu.jpg')}}" data-lightbox="image-1" data-title="Sheltek Properties - 1">
                                    <img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring In Bangladesh">
                                </a>
                            </div>
                           
                        </div>
                    </div>
                                           
                </div>
                <!-- pro-details-description -->
                <div class="pro-details-description products_details mb-50">
                  <h2>PU Floor Coating

</h2>
                    
                    <p class="text-justify">Global Business Solution is the leading PU flooring manufacturer in Bangladesh with rapid curing technologies and experts offers the right PU floor coating services for commercial and industrial applications in Bangladesh.
</h3p>
                    <p class="text-justify">Global Business Solution’s range of polyurethane coating and rapid curing technologies offers everything you need to paint and protect the wood, concrete, and various other types of flooring. We design for you the right PU floor coating solution for anything from commercial to demanding industrial applications. Polyurethane coating is essentially made from two components, polyols, and isocyanates. Added to the combination of these two are various other ingredients or additives. These are mixed on-site and applied within a particular time frame. PU coating is generally applied in a few microns in multiple coats. Our PU floor coating is easy to use and protects the base substrate from heavy foot traffic and chemical impact. Both sustainable, two-component and one-component PU floor coating systems are available. We also offer Solvent-based and PU Hybrid coating(water-based) . Industrial & commercial base floors are often required to bear tremendous dynamic and static loads. Also, they might be vulnerable to continuous abrasion or harsh chemical exposure. Industrial Polyurethane floor coating provides exactly the reliable, long-term protection needed. They keep flooring crack-free and easy to clean and repair – even under demanding conditions. The best part of PU floor coating is they are economical when it comes to recoat or repair as the thickness of the coating is in few microns and can be customized to meet the requirements of specific industrial situations. Properties of  PU floor coating such as elasticity, antibacterial, antifungal can all be adjusted as per the client’s need.</p>
                   
                    <div class="row">
                        <div class="col-md-5">
                            <h2>Epoxy Flooring Uses
</h2>
                            <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Automobiles & Engineering
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Electronics & Electricals
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pharmaceuticals & Laboratory
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Packaging & Plastic Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Chemical Factory
</li>
                                <li><i><i class="fa fa-angle-double-right" aria-hidden="true"></i> Textile Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Paper Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> School & Educational Institutes
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thermal Power Stations
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Milk & Dairies
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meat Processing
</li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <h2>Characteristics/ Advantages
</h2>
                        <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Good Abrasion Resistance

</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Gloss & Matt Options

</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Roll On Coatings
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Fast Dry & Fast Hardness Buildup
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Customizable</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Excellent Chemical Resistance
</li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- widget-featured-property -->
                <aside class="widget widget-featured-property">
                    <h5>Another Services</h5>
                    <div class="row">
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('pucoating')}}"><img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="pu-flooring-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>PU Flooring Solution</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 hidden-sm col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="polished-concrete-in-bangladesh.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>Industrial Epoxy Flooring Solution</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="waterproofing-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>Waterproofing Solution</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="fair-face-plaster.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>Fair Face Plaster</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>

</section>

@endsection
@extends('frontend.layouts.master');
@section('content')


<div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">PU concrete Flooring

</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>PU concrete Flooring

</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- Start page content -->
 <section id="page-content" class="page-wrapper">

<!-- PROPERTIES DETAILS AREA START -->
<div class="properties-details-area pt-115 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- pro-details-image -->
                <div class="pro-details-image mb-60">
                    <div class="pro-details-big-image">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="pro-1">
                                <a href="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" data-lightbox="image-1" data-title="Sheltek Properties - 1">
                                    <img src="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" alt="Epoxy Flooring In Global Business Solution">
                                </a>
                            </div>
                           
                        </div>
                    </div>
                                           
                </div>
                <!-- pro-details-description -->
                <div class="pro-details-description products_details mb-50">
                  <h2>PU concrete Flooring


</h2>
                    
                    <p class="text-justify">Our range of PU concrete flooring is a monolithic, seamless, solvent-free, polyurethane flooring system with a smooth matt finish. Along with high impact resistance, Polyurethane concrete floor coating  can also withstand a good amount of abrasion and chemical resistance.</p>

                    <p class="text-justify">PU concrete floor is also known as one of the toughest in situ floorings when compared with epoxy flooring or regular polyurethane floors. Jemkon’s PU concrete floors generally consist of resinous material with a combination of filler, which includes resin, hardener, filler, and color. Mixed onsite with a variable speed stirrer and applied very quickly due to its short pot life. Unlike regular epoxy or polyurethane coatings, PU concrete floor coating is applied at a higher thickness ranging from 3mm to 12mm depending upon the load factor and chemical resistance required.</p>
                    <p class="text-justify">Our PU concrete floors have a different molecular structure which is well crafted to withstand tremendous dynamic and stationary load and are designed to absorb thermal shock arising due to the temperature fluctuations in cold storage kinds of environment. Steam cleaning is one of the key features of PU concrete flooring which makes it again stand apart from regular floorings..</p>
                    <p class="text-justify">Polyurethane concrete floor is designed to provide long-term maintenance-free PU flooring coating with food-grade properties inhibited which also makes it unique in the epoxy flooring category. Contact Jemkon to get the best estimate of PU concrete flooring service.</p>

                    <div class="row">
                        <div class="col-md-5">
                            <h2>Epoxy Flooring Uses
</h2>
                            <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Automobiles & Engineering
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Electronics & Electricals
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pharmaceuticals & Laboratory
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Packaging & Plastic Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Chemical Factory
</li>
                                <li><i><i class="fa fa-angle-double-right" aria-hidden="true"></i> Textile Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Paper Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> School & Educational Institutes
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thermal Power Stations
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Milk & Dairies
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meat Processing
</li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <h2>Characteristics/ Advantages
</h2>
                        <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Extremely Durable


</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>High Mechanical Strength



</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hygienic
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thermal Shock Resistant

</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Steam Cleaning Resistant
</li>
</li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- widget-featured-property -->
                <aside class="widget widget-featured-property">
                    <h5>Another Services</h5>
                    <div class="row">
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('pucoating')}}"><img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="pu-flooring-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>PU Flooring Solution</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 hidden-sm col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="polished-concrete-in-bangladesh.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>Industrial Epoxy Flooring Solution</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('puself')}}"><img src="{{asset('frontend/assets/images/flat/pusl.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="waterproofing-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>PU Self Level Flooring</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('puconcreate')}}"><img src="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="fair-face-plaster.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>PU concrete Flooring
</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>

</section>

@endsection
@extends('frontend.layouts.master');
@section('content')


<div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">Epoxy Hybrid Flooring Coating




</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li>Epoxy Hybrid Flooring Coating




</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- Start page content -->
 <section id="page-content" class="page-wrapper">

<!-- PROPERTIES DETAILS AREA START -->
<div class="properties-details-area pt-115 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- pro-details-image -->
                <div class="pro-details-image mb-60">
                    <div class="pro-details-big-image">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="pro-1">
                                <a href="{{asset('frontend/assets/images/flat/epoxy.jpg')}}" data-lightbox="image-1" data-title="Sheltek Properties - 1">
                                    <img src="{{asset('frontend/assets/images/flat/epoxy.jpg')}}" alt="Epoxy Flooring In Global Business Solution">
                                </a>
                            </div>
                           
                        </div>
                    </div>
                                           
                </div>
                <!-- pro-details-description -->
                <div class="pro-details-description products_details mb-50">
                  <h2>Epoxy Hybrid Flooring Coating





</h2>
                    
                    <p class="text-justify">Global Business Solution’s Epoxy and Polyurethane hybrid floorings are water-dispersed technology, green initiative chemistry which has very high abrasion resistance, UV resistance. Global Business Solution Epoxy Flooring Coating can be used for wide applications from flooring, epoxy wall coatings, metal structures, epoxy waterproofing system etc.</p>

                    <p class="text-justify">Being water-based and hybrid the pot fife is longer than regular floorings and has the advantage of low odor and low VOC content. Our epoxy and polyurethane hybrid flooring generally consists of resinous material, which includes resin and hardener. These are applied at a thickness ranging from 0.5mm to 3mm, with wide color options available.</p>
                    <p class="text-justify">To get the best epoxy and polyurethane hybrid flooring solution for your industry or commercial use , contact Global Business Solution today to inquire about our Epoxy flooring service. </p>


                    <div class="row">
                        <div class="col-md-5">
                            <h2>Epoxy Flooring Uses
</h2>
                            <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Automobiles & Engineering
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Electronics & Electricals
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pharmaceuticals & Laboratory
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Packaging & Plastic Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Chemical Factory
</li>
                                <li><i><i class="fa fa-angle-double-right" aria-hidden="true"></i> Textile Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Paper Industry
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> School & Educational Institutes
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thermal Power Stations
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Milk & Dairies
</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meat Processing
</li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <h2>Characteristics/ Advantages
</h2>
                        <ul class="">
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Hybrid / Water- Based






</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>UV Resistant






</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>Good Abrasion Resistance



</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thin Coat- Less Repair Cost



</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Excellent Adhesion Properties


</li>
</li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- widget-featured-property -->
                <aside class="widget widget-featured-property">
                    <h5>Another Services</h5>
                    <div class="row">
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('pucoating')}}"><img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="pu-flooring-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>PU Flooring Solution</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 hidden-sm col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="polished-concrete-in-bangladesh.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>Industrial Epoxy Flooring Solution</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                          <div class="flat-item">
                              <div class="flat-item-image">
                              <a href="{{route('puself')}}"><img src="{{asset('frontend/assets/images/flat/pusl.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                  <div class="flat-link">
                                      <a href="waterproofing-in-bangladesh.html">More Details</a>
                                  </div>
                                  <ul class="flat-desc">
                                      <li>
                                          <span>PU Self Level Flooring</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- flat-item -->
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="flat-item">
                                <div class="flat-item-image">
                                <a href="{{route('puconcreate')}}"><img src="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                    <div class="flat-link">
                                        <a href="fair-face-plaster.html">More Details</a>
                                    </div>
                                    <ul class="flat-desc">
                                        <li>
                                            <span>PU concrete Flooring
</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>

</section>

@endsection
@extends('frontend.layouts.master');
@section('content')

<div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                   
<div class="featured-flat">
                        <h2 class="section-title-2 text-center">Industrial & Commercial Flooring</h2>
                        <div class="row">
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('IndustrialEpoxy')}}"><img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="PU Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('IndustrialEpoxy')}}">More Details</a>
                                        </div>

                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Industrial Epoxy Flooring Solution


</span>
                                            </li>
                                            </ul>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('pucoating')}}"><img src="{{asset('frontend/assets/images/flat/pu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('pucoating')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU Floor Coating



</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('puself')}}"><img src="{{asset('frontend/assets/images/flat/pusl.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('puself')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU Self Level Flooring




</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('puconcreate')}}"><img src="{{asset('frontend/assets/images/flat/puconcreate.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('puconcreate')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                PU concrete Flooring





</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epu')}}"><img src="{{asset('frontend/assets/images/flat/epu.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epu')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                EPU Flooring






</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('antistaticepoxy')}}"><img src="{{asset('frontend/assets/images/flat/esdantistatic.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('antistaticepoxy')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Anti Static Epoxy Flooring






</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('EpoxyHybrid')}}"><img src="{{asset('frontend/assets/images/flat/epoxy.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('EpoxyHybrid')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Epoxy Hybrid Flooring Coating







</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('ChemicalResistance')}}"><img src="{{asset('frontend/assets/images/flat/chemical.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('ChemicalResistance')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Chemical Resistance Epoxy Floor








</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('polishedconcretee')}}"><img src="{{asset('frontend/assets/images/flat/concrete.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('polishedconcretee')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Polished Concrete Epoxy Floor


</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epoxycementitiousflooring')}}"><img src="{{asset('frontend/assets/images/flat/cementitiousflooring.jpg')}}" alt="Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epoxycementitiousflooring')}}">More Details</a>
                                        </div>
                                        <ul class="flat-desc">
                                        <li>
                                                <span>
                                                Epoxy Cementitious Flooring


</span>
                                            </li>
                                            </ul>
                                    </div>
                                </div>
                            </div>
                           


                            
                            </div>
                        </div>
                       
                        </div>
                    </div>
                </div>
            </div>

@endsection
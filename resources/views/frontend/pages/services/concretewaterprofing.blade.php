@extends('frontend.layouts.master');
@section('content')
<div class="featured-flat-area pt-115 pb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="section-title-2 text-center">
                            <h2 >Concrete Waterproofing</h2>
                            </div>
                        </div>
                    </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('vinylflooring')}}"><img src="{{asset('frontend/assets/images/flat/11.png')}}" alt="Vinyl Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('vinylflooring')}}">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                           
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('selflevelingepoxy')}}"><img src="{{asset('frontend/assets/images/flat/12.png')}}" alt="Self Leveling Epoxy Flooring"></a>
                                        <div class="flat-link">
                                            <a href="{{route('selflevelingepoxy')}}">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('metallicepoxyflooring')}}"><img src="{{asset('frontend/assets/images/flat/13.png')}}" alt="Metallic Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="{{route('metallicepoxyflooring')}}">More Details</a>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="{{route('epoxyparkingflooring')}}"><img src="{{asset('frontend/assets/images/flat/15.png')}}" alt="Epoxy Parking Solution"></a>
                                        <div class="flat-link">
                                            <a href="{{route('epoxyparkingflooring')}}">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="epoxy-3d-flooring-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/16.png')}}" alt="3D Epoxy Flooring Solutions"></a>
                                        <div class="flat-link">
                                            <a href="epoxy-3d-flooring-in-bangladesh.html">More Details</a>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- flat-item -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="epoxy-wall-coating-and-paint-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/17.png')}}" alt="Epoxy Wall Coating and Paint"></a>
                                        <div class="flat-link">
                                            <a href="epoxy-wall-coating-and-paint-in-bangladesh.html">More Details</a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                           
                            <!-- flat-item -->
                            <div class="col-md-4 hidden-sm col-xs-12">
                                <div class="flat-item">
                                    <div class="flat-item-image">
                                        <a href="floor-hardener-in-bangladesh.html"><img src="{{asset('frontend/assets/images/flat/18.png')}}" alt="Floor Hardener Solutions"></a>
                                        <div class="flat-link">
                                            <a href="floor-hardener-in-bangladesh.html">More Details</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            </div>


                              
                        </div>
                    </div>
                </div>
            </div>

@endsection
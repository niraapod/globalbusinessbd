@extends('frontend.layouts.master');
@section('content')

  <!-- BREADCRUMBS AREA START -->
  <div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">Industrial Epoxy Flooring Solution</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="index.html">Home</a></li>
                                <li>Industrial Epoxy Flooring Solution</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->

        <!-- Start page content -->
        <section id="page-content" class="page-wrapper">

            <!-- PROPERTIES DETAILS AREA START -->
            <div class="properties-details-area pt-115 pb-60">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- pro-details-image -->
                            <div class="pro-details-image mb-60">
                                <div class="pro-details-big-image">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="pro-1">
                                            <a href="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" data-lightbox="image-1" data-title="Sheltek Properties - 1">
                                                <img src="{{asset('frontend/assets/images/flat/EpoxyFlooring.jpg')}}" alt="Epoxy Flooring In Bangladesh">
                                            </a>
                                        </div>
                                       
                                    </div>
                                </div>
                                                       
                            </div>
                            <!-- pro-details-description -->
                            <div class="pro-details-description products_details mb-50">
                              <h2>Industrial Epoxy Flooring Solution
</h2>
                                
                                <p class="text-justify">Global Business Solution the leading industrial epoxy flooring manufacturer in Bangladesh provides best industrial epoxy flooring service in almost all industrial and commercial verticals. Global Business Solution range of industrial epoxy flooring solutions have been applied to more than a million sqft of the area at an affordable cost. Our industrial Epoxy Flooring are the most commonly used in situ types of material, which are also one of the widely accepted epoxy floor coating all over the world because of a variety of characteristics like adhesion, gloss, durability, etc. indusrtrial epoxy flooring generally consists of resinous material with a combination of filler, which includes resin, hardener, filler, and colour. These are mixed on-site and applied within a particular time frame. Epoxy flooring coating has wide options of colour customization are possible and application can be done from as low as few microns to few millimetres. 

</h3p>
                                <p class="text-justify">Global Business Solution epoxy flooring solution is an excellent option for your industrial and commercial project all across Bangladesh. We provide epoxy flooring services all over Bangladesh, including area such as Dhaka, Chittagong, Rajshahi, Khulna and Sylhet etc.   Contact Us to get more information about our industrial epoxy flooring service.</p>
                               
                                <div class="row">
                                    <div class="col-md-5">
                                        <h2>Epoxy Flooring Uses
</h2>
                                        <ul class="">
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Automobiles & Engineering
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Electronics & Electricals
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pharmaceuticals & Laboratory
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Packaging & Plastic Industry
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Chemical Factory
</li>
                                            <li><i><i class="fa fa-angle-double-right" aria-hidden="true"></i> Textile Industry
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Paper Industry
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> School & Educational Institutes
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thermal Power Stations
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Milk & Dairies
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meat Processing
</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Characteristics/ Advantages
</h2>
                                    <ul class="">
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Hospitals & Pharmaceuticals
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Beverages & Food Processing
</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Showrooms</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Manufacturing Facilities</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hotel, Kitchen & Cafeteri</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Aircraft Hangars</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Athletic Flooring</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Garments, Textile & Spinning</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Knitting & Dyeing</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Warehouses & Stores</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Salt & Acid Areas</li>
                                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Any Kinds of Industries</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- widget-featured-property -->
                            <aside class="widget widget-featured-property">
                                <h5>Another Services</h5>
                                <div class="row">
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="flat-item">
                                          <div class="flat-item-image">
                                              <a href="pu-flooring-in-bangladesh.html"><img src="assets/images/flat/pu-flooring.jpg" alt="PU Flooring In Bangladesh"></a>
                                              <div class="flat-link">
                                                  <a href="pu-flooring-in-bangladesh.html">More Details</a>
                                              </div>
                                              <ul class="flat-desc">
                                                  <li>
                                                      <span>PU Flooring Solution</span>
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 hidden-sm col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <a href="polished-concrete-in-bangladesh.html"><img src="assets/images/flat/polished-concrete.jpg" alt="Polished Concrete In Bangladesh"></a>
                                                <div class="flat-link">
                                                    <a href="polished-concrete-in-bangladesh.html">More Details</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <span>Polished Concrete</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="flat-item">
                                          <div class="flat-item-image">
                                              <a href="waterproofing-in-bangladesh.html"><img src="assets/images/flat/waterproofing.jpg" alt="Waterproofing In Bangladesh"></a>
                                              <div class="flat-link">
                                                  <a href="waterproofing-in-bangladesh.html">More Details</a>
                                              </div>
                                              <ul class="flat-desc">
                                                  <li>
                                                      <span>Waterproofing Solution</span>
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>
                                    </div>
                                    <!-- flat-item -->
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="flat-item">
                                            <div class="flat-item-image">
                                                <a href="fair-face-plaster.html"><img src="assets/images/flat/Fair-Face-Plaste.jpg" alt="Fair Face Plaster In Bangladesh"></a>
                                                <div class="flat-link">
                                                    <a href="fair-face-plaster.html">More Details</a>
                                                </div>
                                                <ul class="flat-desc">
                                                    <li>
                                                        <span>Fair Face Plaster</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PROPERTIES DETAILS AREA END -->
        </section>
@endsection
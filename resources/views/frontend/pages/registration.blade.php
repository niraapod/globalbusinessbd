@extends('frontend.layouts.master')
@section('content')
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{Session::get('message')}}
</div>
@endif



<div class="breadcrumbs-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs">
                            <h1 class="breadcrumbs-title">Quotation Page</h1>
                            <ul class="breadcrumbs-list">
                                <li><a href="index.html">Home</a></li>
                                <li>Quotation </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BREADCRUMBS AREA END -->
<br>
<h1 class="text-center">Get a Quotation</h1>

<section id="form_main">
    <div class="container">
        <form class="form" action="" method="" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8" id="form_main_content">
                     <div class="row">
                        <div class="col-md-12" class="form-control">
                            <select class="form-control abc" name="service_res"  id="service_res" required="">
                                <option value="Basement Waterproofing Grade A" a="70">Basement Waterproofing Grade A</option>
                                <option value="Basement Waterproofing Grade B" a="55">Basement Waterproofing Grade B</option>
                                <option value="Exposed Roof Waterproofing 25 Year Warantry" a="130">Exposed Roof Waterproofing 25 Year Warantry</option>
                                <option value="Exposed Roof Waterproofing 15 Year Warantry" a="70">Exposed Roof Waterproofing 15 Year Warantry</option>
                                <option value="Exposed Roof Waterproofing 10 Year Warantry" a="55">Exposed Roof Waterproofing 10 Year Warantry</option>
                                <option value="Exposed Roof Waterproofing 5 Year Warantry" a="35">Exposed Roof Waterproofing 5 Year Warantry</option>
                                <option value="Non -Exposed Roof Waterproofing  20 Year Warantry" a="60">Non -Exposed Roof waterproofing 20 Year Warantry</option>
                                <!--<option value="Roof waterproofing Grade D" a="60">Roof waterproofing Grade D</option>-->
                                <option value="Non -Exposed Roof Waterproofing  10 Year Warantry" a="30">Non -Exposed Roof Waterproofing  10 Year Warantry</option>
                                <option value="Wall Damproofing Solution 5 Year Warantry" a="60">Wall Damproofing Solution 5 Year Warantry</option>
                                <option value="METAL ROOF – Repair & waterproofing solution 5 Year Warantry" a="60">METAL ROOF – Repair & waterproofing solution 5 Year Warantry</option>
                                <option value="METAL ROOF – Repair & waterproofing solution 2 Year Warantry" a="50">METAL ROOF – Repair & waterproofing solution 2 Year Warantry</option>
                                <option value="100% Heat proofing & waterproofing solution 10 Year Warantry" a="250">100% Heat proofing & waterproofing solution 10 Year Warantry </option>
                                <option value="100% Heat proofing & waterproofing solution 5 Year Warantry" a="220">100% Heat proofing & waterproofing solution 5 Year Warantry </option>
                                <option value="Roof Garden & waterproofing solution 5 Year Warantry" a="170">Roof Garden & waterproofing solution 5 Year Warantry.</option>
                                <option value="Industrial PU flooring – 3mm" a="220">Industrial PU flooring – 3mm.</option>
                                <option value="Industrial PU flooring – 4mm" a="230">Industrial PU flooring – 4mm.</option>
                                <option value="Other Report" a="110" selected>Other Report</option>
                                <option value="Other Report" a="110" selected>Select Your Service Requirement</option>
                            </select>
                        </div>
</div>
<br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="company">Company Name/Name*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="basic-addon3"></span> -->
                                <input type="text" class="form-control" id="company" name="company" placeholder="Company/Home Owner" style="width: 100%;" aria-describedby="basic-addon3" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="designation">Designation*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="location"></span> -->
                                <input type="text" id="designation" name="designation" class="form-control" style="width: 100%;" placeholder="Designation" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <label for="address">Address*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="basic-addon3"></span> -->
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" style="width: 100%;" aria-describedby="basic-addon3" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="email">Email*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="location"></span> -->
                                <input type="text" id="email" name="email" class="form-control" style="width: 100%;" placeholder="Email" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="mobile">Mobile no*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="basic-addon3"></span> -->
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile No" style="width: 100%;" aria-describedby="basic-addon3" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="Whatsapp">Whatsapp Number*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="location"></span> -->
                                <input type="text" id="Whatsapp" name="Whatsapp" class="form-control" style="width: 100%;" placeholder="Whatsapp Number" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="project_name">Project Name*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="location"></span> -->
                                <input type="text" id="project_name" name="project_name" class="form-control" style="width: 100%;" placeholder="Project Name" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="location">Project Location*</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="location"></span> -->
                                <input type="text" id="location" name="location" class="form-control" style="width: 100%;" placeholder="Project Location" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="lenth">length * (Feet)</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="length_in_m"></span> -->
                                <input type="number" id="lenth" name="lenth" class="form-control lenth" placeholder="Enter Lenth or Feet" style="width: 100%;" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="width">Width * (Feet)</label>
                            <div class="input-group" style="width: 100%;">
                                <!-- <span class="input-group-addon" id="length_in_m"></span> -->
                                <input type="number" id="width" name="width" class="form-control width" placeholder="Enter width or Feet" style="width: 100%;" aria-describedby="basic-addon2" required="">
                            </div>
                        </div>
                    </div>
                    <br>
                   
                    
                    <div class="row efg">
                         <div class="col-md-6">
                            <label for="tasf">Total area in sft</label>
                            <div class="input-group ta" style="width: 100%;">

                            </div>
                        </div>
                        <div class="col-md-6 hij">
                            <label for="tc">Total Cost</label>
                            <div class="input-group totalcost" id="tc" name="tc" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row" style= text-align:center;>
                        <div class="col-md-12">

                            <button type="submit">Get Quote</button>
                            <p style="font-size: 30px; color:green" id="successmessage"></p>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        //on input lenth calculation
        $('.lenth').on('keyup', function() {
            var lenth = $(this).val()
            var width = $('.width').val()

            var total = lenth * width

            $('.ta').text(total)
        })
        //on input width calculation
        $('.width').on('keyup', function() {
            var width = $(this).val()
            var lenth = $('.lenth').val()

            var total = lenth * width

            $('.ta').text(total)
        })
        //change service reserve calculation
        $('.abc').on('change', function() {
            var sr = $('.abc').find(":selected").attr('a');
            var ta = $('.ta').text()

            sr = parseFloat(sr)
            ta = parseFloat(ta)

            var total = sr * ta


            $('.hij').find('.totalcost').text(total)
        })

        $("form").on("submit", function(event) {
            event.preventDefault();
            var company = $('#company').val()
             var designation = $('#designation').val()
            var address = $('#address').val()
            var email = $('#email').val()
            var mobile = $('#mobile').val()
            var Whatsapp = $('#Whatsapp').val()
            var project_name = $('#project_name').val()
            var location = $('#location').val()
            var lenth = $('#lenth').val()
            var width = $('#width').val()
            var service_res = $('#service_res').val()
            var tasf = parseFloat($('.ta').text())
            var totalcost = parseFloat($('.hij').find('.totalcost').text())
            var costpersf = parseFloat(totalcost / tasf)
            $.ajax({
                url: "{{ route('storeregistration') }}",
                method: "post",
                data: {
                    company,
                    designation,
                    address,
                    email,
                    mobile,
                    Whatsapp,
                    project_name,
                    location,
                    lenth,
                    width,
                    service_res,
                    tasf,
                    totalcost,
                    costpersf,
                    _token: '{{csrf_token()}}'
                },
                success: function(data) {
                        console.log(data)
                if(data.success){
                    // alert(data.message);
                    $('#successmessage').html(data.message)
                   location.replace("http://www.stackoverflow.com");
                }else{
                    alert("Error")
                }

                }
            })
        });
    })
</script>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        /* fonts */
        /* @font-face {
            font-family: 'Segoe UI';
            font-style: normal;
            font-weight: 500;
            src: url("{{ asset('public\\assets\\fonts\\seguisb.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'Segoe UI';
            font-style: normal;
            font-weight: 300;
            src: url("{{ asset('public\\assets\\fonts\\segoeuisl.ttf') }}") format('truetype');
        } */

        html {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-weight: 300;
        }

        body {
            color: #002060;
            line-height: 150%;
            font-weight: 400;
            font-size: 15px;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

        table {
            border-collapse: collapse;
        }

        ul,
        li {
            padding: 0;
            margin: 0;
        }

        .table-bordered,
        .table-bordered td,
        .table-bordered th {
            border: 2px solid #9e9e9e;
            padding: 3px 10px;
            vertical-align: top;
        }

        .w-100 {
            width: 100%;
        }

        .company-birthday {
            position: absolute;
            top: 40px;
            right: 45px;
            transform: translateY(-50%);
            padding: 0 6px;
            line-height: 140%;
            background-color: #6db403;
            color: #ffffff;
            font-size: 12px;
            font-weight: 700;
        }
    </style>
</head>

<body>
   <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 70px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        <div style="color: #00207D;"><b>SERVICE PROVIDER OF</b> </div>

                        <ul style="color: #004CB3;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat Proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                            <li>Repairing </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 60px;">
        <div class="clearfix">
            <div class="f-left" style="width: 75%;">
                <p>To,</p>
            </div>
            <div class="f-right" style="width: 25%;">
                <p>Date:&nbsp;{{ date('d-m-Y') }}</p>
            </div>
        </div>
        <div>
            <div>Designation:&nbsp;{{$designation}}</div>
            <div>Company Name/Name:&nbsp;{{$company}}</div>
            <div>Mobile no:&nbsp;{{$mobile}}</div>
            <div>Address:&nbsp;{{$address}}</div>
            <div>Project Location:&nbsp;{{$location}}</div>
            <div>Whatsapp Number:&nbsp;{{$Whatsapp}}</div>
        </div>

        <div style="margin-top: 20px;">Subject:&nbsp; {{$service_res}}</div>
        <div style="margin-top: 20px;">
            Dear Sir, <br>
            We thank you for inviting us to quote for your prestigious project. We are now pleased to place submit our
            financial proposal with detailed scope of work. <br><br>

            We trust, you would find our proposal for solution in line with your requirement and complete in all
            respect. Should you have any query on any aspect of the proposal, please do not hesitate to contact with
            us.<br><br>

            Thanking and assuring you of our very best co-operation ever, we remain.<br><br><br>


            With Best Regards<br><br><br><br><br>


            MD. WAHIDUR NOBI <br>
            Managing Director
        </div>
    </div>
    <br><br><br><br>
    <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
           
        </tr>
    </table>
    <br>
 <br><br>
 @if($service_res=='Basement Waterproofing Grade A')
    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 70px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        <div style="color: green;"><b>SERVICE PROVIDER OF</b> </div>

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                            <li>Repairing </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: green;">Building Constriction Chemical</div>
                    
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft: 70/- taka</div>
    <strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
                <li>Surface cleaning & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply one coat primer bituminous solvent base primer -“BITUMAT CONCRETE PRIMER” using roller brush,
                    allow 24 hours to dry</li>
                <li>BITUSEAL 1500 CLX - (HDEP, CROSS LAMITION 1.5MM SELF ADHASIVE MEMBRANE) laid by peeling back to
                    protective release film applying the adhesive face onto the prepared surface use roller or hand
                    press on the surface to ensure good bond, aligned and overlapped 50 mm at side & ends.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
            <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
               A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>10
                    years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
        </div>
    </div>
    <br><br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
           
        </tr>
    </table>
    
  @elseif($service_res=='Basement Waterproofing Grade B')
            
    

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                       
<div style="color: green;"><b>SERVICE PROVIDER OF</b> </div>
                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                          
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft: 55/- taka</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface cleaning & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply one coat primer bituminous solvent base primer -“BITUMAT CONCRETE PRIMER” using roller brush,
                    allow 24 hours to dry</li>
                <li>BITUSEAL 1000 laid by peeling back to protective release film applying the adhesive face onto the prepared surface use roller or hand  press on the surface to ensure good bond, aligned and overlapped 50 mm at side & ends.</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>10
                    years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
          
        </tr>
    </table>
    <br><br><br><br><br>
    
     @elseif($service_res=='Exposed Roof Waterproofing 25 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 70px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft: 130/- taka (Undulation, slop-repairing & other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface cleaning & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>‘V” cut for all crack, clean apply primer - Repairing by Hyperseal Expert 150</li>
                <li>Apply one coat PU solvent base primer -“MICROSELAR -50”  using roller brush, allow 24 hours to dry.</li>
                <li>Apply two coat PU membrane “ HYPERDESMO CLASSIC” – as a main coating.</li>
                <li>Apply topcoat – ADYE-  -for protect main coating & for UV protection .</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>25 years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
     @elseif($service_res=='Exposed Roof Waterproofing 15 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft: 70/- taka (Undulation, slop-repairing & other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface cleaning & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>‘V” cut for all crack, clean apply primer - Repairing by Hyperseal Expert 150</li>
                <li>Apply one coat PU solvent base primer -“MICROSELAR -50”  using roller brush, allow 24 hours to dry.</li>
                <li>Apply two coat PU membrane “ HYPERDESMO CLASSIC” – as a main coating.</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>15 years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
    @elseif($service_res=='Exposed Roof Waterproofing 10 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  55/- taka (Undulation, slop-repairing & other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>‘V” cut for all crack, clean apply primer - Repairing by BITUMAT NEWSEAL PRO PU SELANT</li>
                <li>Apply one coat Acrylic coating base primer -“SYNROOF -HB”  using roller brush, allow 12 hours to .</li>
                <li>Apply one coat Acrylic membrane “SYNROOF -HB” with 0.5mm geo-textile or 45gsm glass fiber.</li>
                <li>Apply one coat Acrylic membrane “SYNROOF -HB” as a top coat.</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>10  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
    @elseif($service_res=='Exposed Roof Waterproofing 5 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  35/- taka (Undulation, slop-repairing & other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply acrylic cementation coating “IZOFLAX -7000</li>
                <li>Apply Acrylic membrane “SYNROOF -HB” </li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
     @elseif($service_res=='Non -Exposed Roof Waterproofing  20 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  60/- taka (Undulation, slop-repairing & other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. </li>
                <li>Site preparation and Site clearance for the work inside by us and outside the building shall be arranged by you as and when requested</li>
                <li>You shall provide scaffolding & Lifting facility wherever required, free of cost</li>
                <li>Access to reach the top floor/ Basement should be provided by the client</li>
                <li>You will provide secure storage facility for materials and equipment at site.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>20  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
     @elseif($service_res=='Non -Exposed Roof Waterproofing  10 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  30/- taka (crack repair, Undulation, slop-repairing paden stone cost, other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>)Apply 2 coat acrylic cementation coating “IZOFLAX -7000 </li>
                <li>Top of coating make a sand finishing – with silica sand, </li>
                <li>Cover it by new Paden stone casting within 72 hours</li>
                <li>Carry out flood test for 72 hours</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
    @elseif($service_res=='Wall Damproofing Solution 5 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  60/- taka (crack repair, Undulation, slop-repairing paden stone cost, other civil work cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Removed the wall old plaster – the damage area  </li>
                <li>Apply 2 coat acrylic cementation coating “IZOFLAX -7000 </li>
                <li>Re- plaster the repair area.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
    @elseif($service_res=='METAL ROOF – Repair & waterproofing solution 5 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  60/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface cleaning- Metal Roof Surface must be clean, even, and free from old paint & loose particles removed felt </li>
                <li>Re Fixing: Removed the damage nut & refixed new nut some place requires need drill & rebate fixed the metal strongly if require put new metal sheet if require change the gutter. </li>
                <li>Joint treatment:  Providing PU sealant- “HYPERSEAL EXPERT” for gutter edges joints, filling material and PU sealant application inside the top truss cap and sheet joints.  </li>
                <li>Laying textile:  6 inch geotextile laying all over the over lapping joints with 2 coat acrylic coating </li>
                <li>Primer Coating:  apply “SYNROOF -HB acrylic coating for primer hole metal roof</li>
                <li>2 coat: Hole metal roof apply “SYNROOF -HB acrylic coating.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    @elseif($service_res=='METAL ROOF – Repair & waterproofing solution 2 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  50/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface cleaning- Metal Roof Surface must be clean, even, and free from old paint & loose particles removed felt </li>
                <li>Re Fixing: Removed the damage nut & refixed new nut some place requires need drill & rebate fixed the metal strongly if require put new metal sheet if require change the gutter </li>
                <li>Joint treatment:  Providing PU sealant- “HYPERSEAL EXPERT” for gutter edges joints, filling material and PU sealant application inside the top truss cap and sheet joints. </li>
                <li>Laying textile:  6 inch geotextile laying all over the over lapping joints with 2 coat acrylic coating </li>
                <li>Laying textile:  6 inch geotextile laying all over the over lapping joints with 2 coat acrylic coating .</li>
                <li>Aluminum membrane: Apply IZOSELF – ALU.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>2  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    @elseif($service_res=='100% Heat proofing & waterproofing solution 10 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  250/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply acrylic cementation coating “IZOFLAX -7000 </li>
                <li>Apply XPS Board, adjust Board mail female group properly.</li>
                <li>Develop 2.5” new screed 10’X 10’ block with 6mm expansion joint applying Hyperseal Expert-150. (Quantity as per MSDS)</li>
                <li>Apply 2 coat acrylic cementation coating “IZOFLAX -7000 with top coat SYNROOF -HB</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>10  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
     @elseif($service_res=='100% Heat proofing & waterproofing solution 5 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  220/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply two coat PU membrane “FLEXI MAXX PU 1K”  ” – on RCC slab for waterproofing</li>
                <li>Laying of average 75mm thickness foam concrete for temperature control </li>
                <li>Develop 2.5” new screed 10’X 10’ block with 6mm expansion joint applying Hyperseal Expert-150. (Quantity as per MSDS)</li>
                <li>Over the foam concrete apply 2 coat acrylic cementation coating “IZOFLAX -7000 with top coat SYNROOF -HB</li>
                <li>Carry out flood test for 72 hours.</li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    @elseif($service_res=='Roof Garden & waterproofing solution 5 Year Warantry')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  220/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 14px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply two coat PU membrane “HYPERDESMO – PB 2K”  ” – on RCC slab for waterproofing</li>
                <li>Laying of 8mm dimple board with geo-textile  </li>
                <li>Over the put silica sand after that preparing the garden </li>
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Electrical power supply for welding, cutting etc. shall be provided by you free of cost at site. <br>
                3) Site preparation and Site clearance for the work inside by us and outside the building shall be
                arranged by you as and when requested <br>
                4) You shall provide scaffolding & Lifting facility wherever required, free of cost. <br>
                5). Access to reach the top floor/ Basement should be provided by the client <br>
                6) You will provide secure storage facility for materials and equipment at site. <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
              
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div>The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    @elseif($service_res=='Industrial PU flooring – 3mm')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  170/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 10px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply two coat PU membrane “HYPERDESMO – PB 2K”  ” – on RCC slab for waterproofing</li>
                
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Apply primer, PU coating (as require on qutation) apply top coat on the top of PU coating. <br>
                 <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
               <div style="padding-left: 50px; font-size: 10px;">
                1.The above Quotation has been Prepared with Consideration of a standard Surface. If Surface has excessive undulation, additional product might be required, which would be notified prior application for client's approval
                <br>
                2.	The offer and terms of the offer provided for inclusive of PU material, Transportation of material,
     	Manpower, tools and plant are indicative and it’s confirmed by one our applicator after site inspection

                <br>
              3.	Floor Preparation: If needed civil works of the floor will be done by client before paint application process starts Clients has to confirm the strength the floor before start the paint work. Paint consumption may vary depending on surface condition e.g unevenness of floor, big hole or damage of floor. Undulation of the floor more than the thickness of paint can't cover by paint.
              <br>
              4.	Painting Environment: Sufficient Light, Air (Ventilation), dust free environment should be arrange by client at the time of paint application. Proper measure should be taken by client for insect control during paint application. Any kind of water Spillage is not allowed during floor preparation and paint application Any kind of maintenance and operational work will not allowed during paint application.  Area Measurement: Final Bill will be submitted as per actual measurement.
            </div>
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px; font-size: 10px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team<br>
                D.Vat & Tax will be not included.<br>
                E.Actual measurement will be calculated for the bill.<br>
                F. Water and electricity required for the work as well as accommodation for our manpower shall be supplied by you free at site<br>
                G. Space for storage of materials and built-in premises for our tools, plants etc. shall be provided by you, free at site.<br>
                H.	You shall provide scaffolding & Lifting facility wherever required, free of cost.<br>
                I.  Access to reach the top floor should be provided by the client<br>
                j.This quotation will be valid 30th days from the date of issued
                <br>
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div style="font-size: 10px;">The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px; font-size: 10px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
    
      @elseif($service_res=='Industrial PU flooring – 4mm')
            

    <div class="header">
        <table>
            <tr>
                <td style="width: 380px; position: relative;">
                    <div class="company-birthday">SINCE: 2009</div>
                    <img src="{{ asset('public/logo.png') }}" alt="" style="height: 50px;">
                </td>
                <td rowspan="2">
                    <!-- margin-left: 100px; background-color: #d8d8d8; -->
                    <div style="margin-left: 50px; width: 200px; display: inline-block;">
                        

                        <ul style="color: green;">
                            <li>Concrete Waterproofing</li>
                            <li>Heat proofing</li>
                            <li>Flooring </li>
                            <li>Paint & Coating</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="color: #6db403;">Building Constriction Chemical</div>
                </td>
            </tr>
        </table>

    </div>

    <div class="container" style="padding: 0 30px;">
        <strong>QUTATION:</strong> <br>
        <div>Total area in sqf:&nbsp; {{$tasf}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Length in Feet:&nbsp; {{$lenth}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Width in sqf: &nbsp; {{$width}}</div>
        <div>Price per sft:  230/- taka (cost of extra metal sheet & gutter cost excluded)</div>
<strong>TOTAL COST:&nbsp; {{$totalcost}}</strong> <br>
        <div style="margin-top: 10px;"><strong>Price including-</strong></div>
    </div>

    <div class="container" style="padding: 0 30px;">
        <div style="font-size: 10px; line-height: 120%; text-align: justify;">
            Total value: lording- unloading, Transportation, Mobilization cost, surface cleaning, cost of primer,
            applying primer, laying self-adhesive membrane <br>
            <strong>Application Methodology: </strong> <br>
            <ul>
               <li>Surface deep grinding & free of dirt, gravel, dust & oil make it completely cured & dried</li>
                <li>Apply two coat PU membrane “HYPERDESMO – PB 2K”  ” – on RCC slab for waterproofing</li>
                
            </ul>

            <div><strong>NOTES:</strong></div>
            <div>
                1) Accommodation of our Supervisory staff and other personnel shall be arranged by you at site. <br>
                2) Apply primer, PU coating (as require on qutation) apply top coat on the top of PU coating. <br>
                 <br>
            </div>

            <div style="margin-top: 4px;"><strong>TERMS AND CONDITIONS:</strong></div>
               <div style="padding-left: 50px; font-size: 10px;">
                1.The above Quotation has been Prepared with Consideration of a standard Surface. If Surface has excessive undulation, additional product might be required, which would be notified prior application for client's approval
                <br>
                2.	The offer and terms of the offer provided for inclusive of PU material, Transportation of material,
     	Manpower, tools and plant are indicative and it’s confirmed by one our applicator after site inspection

                <br>
              3.	Floor Preparation: If needed civil works of the floor will be done by client before paint application process starts Clients has to confirm the strength the floor before start the paint work. Paint consumption may vary depending on surface condition e.g unevenness of floor, big hole or damage of floor. Undulation of the floor more than the thickness of paint can't cover by paint.
              <br>
              4.	Painting Environment: Sufficient Light, Air (Ventilation), dust free environment should be arrange by client at the time of paint application. Proper measure should be taken by client for insect control during paint application. Any kind of water Spillage is not allowed during floor preparation and paint application Any kind of maintenance and operational work will not allowed during paint application.  Area Measurement: Final Bill will be submitted as per actual measurement.
            </div>
              <div style="font-size: 14px;"><strong>Payment Terms:</strong></div>
            <div style="padding-left: 50px; font-size: 10px;">
                A. 10% of the value of the contract shall be paid to us as mobilization advance along with the work
                order and same shall be recovered from our RA bills on pro-rata basis.<br>
                B. 80% of the value of material portion shall be paid by you against supply of material at site with due
                submission of invoice and proof of supply within 3 days.<br>
                C. Balance 10% against completion of work within 10 days on submission of RA bills with necessary check
                list and measurement sheets duly certified by your site team<br>
                D.Vat & Tax will be not included.<br>
                E.Actual measurement will be calculated for the bill.<br>
                F. Water and electricity required for the work as well as accommodation for our manpower shall be supplied by you free at site<br>
                G. Space for storage of materials and built-in premises for our tools, plants etc. shall be provided by you, free at site.<br>
                H.	You shall provide scaffolding & Lifting facility wherever required, free of cost.<br>
                I.  Access to reach the top floor should be provided by the client<br>
                j.This quotation will be valid 30th days from the date of issued
                <br>
            </div>

            <div style="margin-top: 4px; font-size: 17px;"><strong>WARRANTY:</strong></div>
            <div style="font-size: 10px;">The item of waterproofing work shall carry a Warranty for good performance for a period of <strong>5  years</strong>. Our liability shall be restricted to the cost of rectifying any defect in our
                waterproofing works and does not cover any other damages. The warranty will be given on receipt of
                payment of our final bill and retention deposit in full and final settlement.</div>

            <div style="margin-top: 8px; font-size: 10px;"><strong>PAYMENT:</strong></div>
            <div>
                CUSTOMER shall pay the AGREEMENT PRICE and all other payments agreed herein to GLOBAL BUSINESS SOLUTION
                –all payment made by AC. payee cheque.
            </div>
          
        </div>
    </div>
    
    <br>
 <table  style="font-size: 10px; line-height: 120%; color: #000;">
        <tr>
            <td width="30%">
                <img src="{{ asset('public/qr.png') }}" alt="" style="height: 50px;">
                
            </td>
            <td style="width: 35%;">
                <strong>Registered Office:</strong> <br>
                198-202, Nawabpur
                Tower, Room no# 311 
                Nawabpur Road 
                Nawabpur, 
                Dhaka-1100 
                Bangladesh
            </td>
           
            <td style="width: 35%;">
                <strong>Branch Office:</strong> <br>
                Dewanhat City 
                Corporation College, 
                Dewanhat
                Overbridge
                Chattogram 4000
            </td>
        
        </tr>
    </table>
@else
Abdul Goni
@endif
</body>

</html>


<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Backend\ProfileController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\LogoController;
use App\Http\Controllers\Backend\BannerController;
use App\Http\Controllers\Backend\ContactController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\ProductControllerTwo;
use App\Http\Controllers\Backend\ClientController;
use App\Http\Controllers\Backend\AcrylicCoating;
use App\Http\Controllers\Backend\OurProjectController;
use App\Http\Controllers\Backend\FlooringController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('backend.layouts.home');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


Route::get('/', [HomeController::class, 'index' ]);
route::post('contact/store', [HomeController::class, 'contactstore' ])->name('contact.store');
route::post('dellar/store', [HomeController::class, 'dellar' ])->name('dellar.store');
Route::post('storeregistration', [HomeController::class, 'storeregistration' ])->name('storeregistration');
Route::get('/about/global-business/', [HomeController::class, 'globalbusiness' ])->name('globalbusiness');
// Route::get('/about/mision-vision/', [HomeController::class, 'misionvision' ])->name('misionvision');
// Route::get('/about/sustainability/', [HomeController::class, 'sustainability' ])->name('sustainability');
// Route::get('/about/careers/', [HomeController::class, 'careers' ])->name('careers');
Route::get('contact-us', [HomeController::class, 'contact' ])->name('contact');
Route::get('calculator', [HomeController::class, 'registration' ])->name('registration');
Route::get('service', [HomeController::class, 'service' ])->name('service');
Route::get('product-list', [HomeController::class, 'productlist' ])->name('productlist');
Route::get('project', [HomeController::class, 'project' ])->name('project');
Route::get('service/polished-concrete', [HomeController::class, 'polishedconcrete' ])->name('polishedconcrete');
Route::get('service/pu-flooring', [HomeController::class, 'puflooring' ])->name('puflooring');
Route::get('service/vinyl-flooring', [HomeController::class, 'vinylflooring' ])->name('vinylflooring');
Route::get('service/epoxy-flooring', [HomeController::class, 'epoxyflooring' ])->name('epoxyflooring');
Route::get('service/self-leveling-epoxy', [HomeController::class, 'selflevelingepoxy' ])->name('selflevelingepoxy');
Route::get('service/metallic-epoxy-flooring', [HomeController::class, 'metallicepoxyflooring' ])->name('metallicepoxyflooring');
Route::get('fair-face-plaster', [HomeController::class, 'fairfaceplaster' ])->name('fairfaceplaster');
Route::get('waterproofing', [HomeController::class, 'waterproofing' ])->name('waterproofing');
Route::get('constructionchemicals', [HomeController::class, 'constructionchemicals' ])->name('constructionchemicals');
Route::get('epoxy-parking-flooring', [HomeController::class, 'epoxyparkingflooring' ])->name('epoxyparkingflooring');
Route::get('companyprofile', [HomeController::class, 'companyprofile' ])->name('companyprofile');


Route::get('flooring', [HomeController::class, 'flooring' ])->name('flooring');
Route::get('Industrial-Epoxy-Flooring', [HomeController::class, 'IndustrialEpoxy' ])->name('IndustrialEpoxy');
Route::get('pucoating', [HomeController::class, 'pucoating' ])->name('pucoating');
Route::get('PU-Self-Level-Flooring', [HomeController::class, 'puself' ])->name('puself');
Route::get('pu-concreate', [HomeController::class, 'puconcreate' ])->name('puconcreate');
Route::get('pu-concreate', [HomeController::class, 'puconcreate' ])->name('puconcreate');
Route::get('epu', [HomeController::class, 'epu' ])->name('epu');
Route::get('Anti-Static-Epoxy', [HomeController::class, 'antistaticepoxy' ])->name('antistaticepoxy');
Route::get('Epoxy-Hybrid', [HomeController::class, 'EpoxyHybrid' ])->name('EpoxyHybrid');
Route::get('Chemical-Resistance', [HomeController::class, 'ChemicalResistance' ])->name('ChemicalResistance');
Route::get('Polished-Concrete', [HomeController::class, 'polishedConcretee'])->name('polishedconcretee');
Route::get('epoxy-cementitious-flooring', [HomeController::class, 'epoxycementitiousflooring'])->name('epoxycementitiousflooring');

Route::get('concretewaterprofing', [HomeController::class, 'concretewaterprofing' ])->name('concretewaterprofing');
Route::get('PAINTCOATING', [HomeController::class, 'PAINTCOATING' ])->name('PAINTCOATING');
Route::get('epoxycoating', [HomeController::class, 'epoxycoating' ])->name('epoxycoating');

Route::get('repearing', [HomeController::class, 'repearing' ])->name('repearing');
Route::get('hetprofing', [HomeController::class, 'hetprofing' ])->name('hetprofing');

//backend
Route::group(['middleware'=>'auth'], function(){
	Route::prefix('users')->group(function(){
	route::get('/view',[UserController::class, 'view'])->name('users.view');
	route::get('/add', [UserController::class, 'add'])->name('users.add');
	route::post('/store', [UserController::class, 'store'])->name('users.store');
	route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
	route::post('/update/{id}', [UserController::class, 'update'])->name('users.update');
	route::get('/delete/{id}', [UserController::class, 'delete'])->name('users.delete');
});

Route::prefix('profile')->group(function(){
	route::get('/view',[ProfileController::class,'view'])->name('profile.view');
	route::get('/password/view', [ProfileController::class,'passwordView'])->name('password.view');
	route::post('/store', [ProfileController::class,'store'])->name('profile.store');
	route::get('/edit', [ProfileController::class,'edit'])->name('profile.edit');
	route::post('/update', [ProfileController::class,'update'])->name('profile.update');
	route::get('/delete/{id}', [ProfileController::class,'delete'])->name('profile.delete');
	route::post('/password/update', [ProfileController::class,'passwordupdate'])->name('password.update.view');
});

Route::prefix('logos')->group(function(){
	route::get('/view', [LogoController::class,'view'])->name('logos.view');
	route::get('/add', [LogoController::class,'add'])->name('logos.add');
	route::post('/store', [LogoController::class,'store'])->name('logos.store');
	route::get('/edit/{id}', [LogoController::class,'edit'])->name('logos.edit');
	route::post('/update/{id}', [LogoController::class,'update'])->name('logos.update');
	route::get('/delete/{id}', [LogoController::class,'delete'])->name('logos.delete');
});

Route::prefix('sliders')->group(function(){
	route::get('/view', [BannerController::class,'view'])->name('sliders.view');
	route::get('/add', [BannerController::class,'add'])->name('sliders.add');
	route::post('/store', [BannerController::class,'store'])->name('sliders.store');
	route::get('/edit/{id}', [BannerController::class,'edit'])->name('sliders.edit');
	route::post('/update/{id}', [BannerController::class,'update'])->name('sliders.update');
	route::get('/delete/{id}', [BannerController::class,'delete'])->name('sliders.delete');
});

Route::prefix('contacts')->group(function(){
	route::get('/view', [ContactController::class,'view'])->name('contacts.view');
	route::get('/add', [ContactController::class,'add'])->name('contacts.add');
	route::post('/store', [ContactController::class,'store'])->name('contacts.store');
	route::get('/edit/{id}', [ContactController::class,'edit'])->name('contacts.edit');
	route::post('/update/{id}', [ContactController::class,'update'])->name('contacts.update');
	route::get('/delete/{id}', [ContactController::class,'delete'])->name('contacts.delete');
	route::get('communicate/delete/{id}', [ContactController::class,'communicatedelete'])->name('communicate.delete');
	route::get('/communicate', [ContactController::class,'viewCommunicate'])->name('contacts.communicate');
	route::get('delllarship/delete/{id}', [ContactController::class,'delllarshipdelete'])->name('deller.delete');
	route::get('/delllarship', [ContactController::class,'viewdelllarship'])->name('contacts.deller');
	route::get('calcutor/delete/{id}', [ContactController::class,'calcutordelete'])->name('calculator.delete');
	route::get('/calcutor', [ContactController::class,'viewdcalcutor'])->name('contacts.calcutor');
});


Route::prefix('products')->group(function(){
	route::get('/view', [ProductController::class,'view'])->name('products.view');
	route::get('/add', [ProductController::class,'add'])->name('products.add');
	route::post('/store', [ProductController::class,'store'])->name('products.store');
	route::get('/edit/{id}', [ProductController::class,'edit'])->name('products.edit');
	route::post('/update/{id}', [ProductController::class,'update'])->name('products.update');
	route::get('/delete/{id}', [ProductController::class,'delete'])->name('products.delete');
});


Route::prefix('productss')->group(function(){
	route::get('/view', [ProductControllerTwo::class,'view'])->name('productss.view');
	route::get('/add', [ProductControllerTwo::class,'add'])->name('productss.add');
	route::post('/store', [ProductControllerTwo::class,'store'])->name('productss.store');
	route::get('/edit/{id}', [ProductControllerTwo::class,'edit'])->name('productss.edit');
	route::post('/update/{id}', [ProductControllerTwo::class,'update'])->name('productss.update');
	route::get('/delete/{id}', [ProductControllerTwo::class,'delete'])->name('productss.delete');
});

Route::prefix('clients')->group(function(){
	route::get('/view', [ClientController::class,'view'])->name('clients.view');
	route::get('/add', [ClientController::class,'add'])->name('clients.add');
	route::post('/store', [ClientController::class,'store'])->name('clients.store');
	route::get('/edit/{id}', [ClientController::class,'edit'])->name('clients.edit');
	route::post('/update/{id}', [ClientController::class,'update'])->name('clients.update');
	route::get('/delete/{id}', [ClientController::class,'delete'])->name('clients.delete');
});

Route::prefix('acryliccoatings')->group(function(){
	route::get('/view', [AcrylicCoating::class,'view'])->name('acryliccoatings.view');
	route::get('/add', [AcrylicCoating::class,'add'])->name('acryliccoatings.add');
	route::post('/store', [AcrylicCoating::class,'store'])->name('acryliccoatings.store');
	route::get('/edit/{id}', [AcrylicCoating::class,'edit'])->name('acryliccoatings.edit');
	route::post('/update/{id}', [AcrylicCoating::class,'update'])->name('acryliccoatings.update');
	route::get('/delete/{id}', [AcrylicCoating::class,'delete'])->name('acryliccoatings.delete');
});

Route::prefix('floorings')->group(function(){
	route::get('/view', [FlooringController::class,'view'])->name('floorings.view');
	route::get('/add', [FlooringController::class,'add'])->name('floorings.add');
	route::post('/store', [FlooringController::class,'store'])->name('floorings.store');
	route::get('/edit/{id}', [FlooringController::class,'edit'])->name('floorings.edit');
	route::post('/update/{id}', [FlooringController::class,'update'])->name('floorings.update');
	route::get('/delete/{id}', [FlooringController::class,'delete'])->name('floorings.delete');
});


Route::prefix('projects')->group(function(){
	route::get('/view', [OurProjectController::class,'view'])->name('projects.view');
	route::get('/add', [OurProjectController::class,'add'])->name('projects.add');
	route::post('/store', [OurProjectController::class,'store'])->name('projects.store');
	route::get('/edit/{id}', [OurProjectController::class,'edit'])->name('projects.edit');
	route::post('/update/{id}', [OurProjectController::class,'update'])->name('projects.update');
	route::get('/delete/{id}', [OurProjectController::class,'delete'])->name('projects.delete');
});

});

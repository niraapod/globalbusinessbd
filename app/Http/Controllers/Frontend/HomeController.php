<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Communicate;
use App\Models\Dellership;
use App\Models\Registration;
use App\Models\Product;
use App\Models\Client;
use App\Models\ProductTwo;
use App\Models\Acrylic;
use App\Models\OurProject;
use App\Models\Flooring;
use PDF;
use Mail;

class HomeController extends Controller
{
    public function index(){
        $data['alldata'] = Client::all();
    	return view('frontend.layouts.home',$data);
    }

    public function globalbusiness(){
    	return view('frontend.pages.globalbusiness');
    }

    public function misionvision(){
    	return view('frontend.pages.misionvision');
    }


    public function storeregistration(Request $req){
        $data = new Registration;
        $data->company = $req->company;
        $data->designation = $req->designation;
        $data->address = $req->address;
        $data->email = $req->email;
        $data->mobile = $req->mobile;
        $data->Whatsapp = $req->Whatsapp;
        $data->project_name = $req->project_name;
        $data->location = $req->location;
        $data->lenth = $req->lenth;
        $data->width = $req->width;
        $data->service_res = $req->service_res;
        $data->tasf = $req->tasf;
        $data->totalcost = $req->totalcost;
        $data->costpersf = $req->costpersf;
        $save = $data->save();

        $data = array(
            'company' => $req->company,
            'address' => $req->address,
            'email'=>$req->email,
            'mobile'=>$req->mobile,
            'Whatsapp'=>$req->Whatsapp,
            'designation'=>$req->designation,
            'location'=>$req->location,
            'lenth'=>$req->lenth,
            'width'=>$req->width,
            'service_res'=>$req->service_res,
            'tasf'=>$req->tasf,
            'totalcost'=>$req->totalcost,
            'costpersf'=>$req->costpersf,
        );
        
        $pdf = PDF::loadView('frontend.emails.calculator', $data);

        Mail::send('frontend.emails.contact', $data, function($message) use($data, $pdf){
            $message->from('info@globalbusinessbd.com', 'Global Business Solution');
            $message->to($data['email']);
            $message->subject('Thanks for contact us')
            ->attachData($pdf->output(), "gbs_quotation.pdf");
        });
        return response()->json(
            [
                'success' => true,
                'message' => 'please check your email'
            ]
        );
    
    }

     public function sustainability(){
    	return view('frontend.pages.sustainability');
    }

    public function careers(){
    	return view('frontend.pages.careers');
    }

    public function contact(){
    	return view('frontend.pages.contact');
    }

    public function registration(){
    	return view('frontend.pages.registration');
    }

    public function productlist(){
        $data['alldata'] = Product::all();
        $data['alldataa'] = ProductTwo::all();
        $data['alldataaa'] = Acrylic::all();
        $data['flooring'] = Flooring::all();
    	return view('frontend.pages.productlist',$data);
    }

    public function service(){
    	return view('frontend.pages.service');
    }

    public function polishedconcrete(){
    	return view('frontend.pages.services.polishedconcrete');
    }

    public function puflooring(){
    	return view('frontend.pages.services.puflooring');
    }

    public function vinylflooring(){
    	return view('frontend.pages.services.vinylflooring');
    }

    public function epoxyflooring(){
    	return view('frontend.pages.services.epoxyflooring');
    }

    public function selflevelingepoxy(){
    	return view('frontend.pages.services.selflevelingepoxy');
    }

    public function metallicepoxyflooring(){
    	return view('frontend.pages.services.metallicepoxyflooring');
    }

    public function project(){
        $data['alldata'] = OurProject::all();
    	return view('frontend.pages.project', $data);
    }


    public function fairfaceplaster(){
    	return view('frontend.pages.fairfaceplaster');
    }

    public function waterproofing(){
    	return view('frontend.pages.waterproofing');
    }




    public function constructionchemicals(){
    	return view('frontend.pages.constructionchemicals');
    }


    public function epoxyparkingflooring(){
    	return view('frontend.pages.services.epoxyparkingflooring');
    }
    

    public function companyprofile(){
    	return view('frontend.pages.services.companyprofile');
    }


    public function flooring(){
    	return view('frontend.pages.services.flooring');
    }


    public function concretewaterprofing(){
    	return view('frontend.pages.services.concretewaterprofing');
    }

    public function PAINTCOATING(){
    	return view('frontend.pages.services.PAINTCOATING');
    }

    public function epoxycoating(){
    	return view('frontend.pages.services.epoxycoating');
    }

    public function pucoating(){
    	return view('frontend.pages.services.puflooring');
    }

    public function repearing(){
    	return view('frontend.pages.services.repearing');
    }

    public function hetprofing(){
    	return view('frontend.pages.services.hetprofing');
    }

    public function IndustrialEpoxy(){
    	return view('frontend.pages.services.IndustrialEpoxy');
    }

    public function puself(){
    	return view('frontend.pages.services.puself');
    }

    public function puconcreate(){
    	return view('frontend.pages.services.puconcreate');
    }

    public function epu(){
    	return view('frontend.pages.services.epu');
    }

    public function antistaticepoxy(){
    	return view('frontend.pages.services.antistaticepoxy');
    }

    public function EpoxyHybrid(){
    	return view('frontend.pages.services.EpoxyHybrid');
    }

    public function ChemicalResistance(){
    	return view('frontend.pages.services.ChemicalResistance');
    }

    public function polishedconcretee(){
    	return view('frontend.pages.services.polishedconcretee');
    }

    public function epoxycementitiousflooring(){
    	return view('frontend.pages.services.epoxycementitiousflooring');
    }


    public function contactstore(Request $request){
        $contact = new Communicate();
        $contact->f_name = $request->f_name;
        $contact->l_name = $request->l_name;
        $contact->email = $request->email;
        $contact->mobile_no = $request->mobile_no;
        $contact->country = $request->country;
        $contact->city = $request->city;
        $contact->msg = $request->msg;
        $contact->save();

        $data = array(
            'f_name' => $request->f_name,
            'email'=>$request->email,
            'mobile_no'=> $request->mobile_no,
            'country' => $request->country,
            'msg' => $request->msg,
        );
        $pdf = PDF::loadView('frontend.emails.contact', $data);

        Mail::send('frontend.emails.contact', $data, function($message) use($data, $pdf){
            $message->from('abdulgoni.me@gmail.com', 'Abdul Goni');
            $message->to($data['email']);
            $message->subject('Thanks for contact us')
            ->attachData($pdf->output(), "text.pdf");

        });
        return redirect()->back()->with('success', 'Your message successfully sent');

    }

    public function dellar(Request $request){
        $contact = new Dellership();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->mobile_no = $request->mobile_no;
        $contact->service_area = $request->service_area;
        $contact->save();
        return redirect()->back()->with('success', 'Your Dellarship request successfully sent');

    }

}

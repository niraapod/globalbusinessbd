<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Flooring;

class FlooringController extends Controller
{
    public function view(){
     
      
        $data['flooring'] = Flooring::all();
        return view('backend.productsss.productsss-view', $data);
}
public function add(){
  return view('backend.productsss.add-productsss');
}

public function store(Request $request){
  $data = new Flooring();
  $data->created_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {
  if ($request->file('image')) {
    $file = $request->file('image');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/productsss_images'), $filename);
    $data['image'] = $filename;
}
}

if($request->hasFile('technical_datasheet')) {
  if ($request->file('technical_datasheet')) {
    $file = $request->file('technical_datasheet');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/technicalsss_datasheet'), $filename);
    $data['technical_datasheet'] = $filename;
}
}

if($request->hasFile('test_report')) {
  if ($request->file('test_report')) {
      $file = $request->file('test_report');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/testreportsss'), $filename);
      $data['test_report'] = $filename;
    }
  }
  $data->save();
  return redirect()->route('floorings.view')->with('message', 'Product add successfully');

}



public function edit($id){
$editData = Flooring::find($id);
return view ('backend.productsss.edit-productsss', compact('editData'));
}

public function update(Request $request, $id){
  $data = Flooring::find($id);
  $data->updated_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {
  if ($request->file('image')) {
      $file = $request->file('image');
      @unlink(public_path('upload/productsss_images/'.$data->image));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/productsss_images'), $filename);
      $data['image'] = $filename;
    }
  }
    if($request->hasFile('technical_datasheet')) {
      if ($request->file('technical_datasheet')) {
        $file = $request->file('technical_datasheet');
        @unlink(public_path('upload/technicalsss_datasheet/'.$data->technical_datasheet));
        $filename =$file->getClientORiginalName();
        $file->move(public_path('upload/technicalsss_datasheet'), $filename);
        $data['technical_datasheet'] = $filename;
    }
    }
    
    if($request->hasFile('test_report')) {
      if ($request->file('test_report')) {
          $file = $request->file('test_report');
          @unlink(public_path('upload/testreportsss/'.$data->test_report));
          $filename =$file->getClientORiginalName();
          $file->move(public_path('upload/testreportsss'), $filename);
          $data['test_report'] = $filename;
        }
      }


      $data->save();
  return redirect()->route('floorings.view')->with('message', 'Product update successfully');

}
public function delete($id){
       $data = Flooring::find($id);
       if (file_exists('/upload/productsss_images/' .$data->image) AND ! empty($data->image)) {
            unlink('/upload/productsss_images/' . $data->image);
        }

        if (file_exists('/upload/technicalsss_datasheet/' .$data->technical_datasheet) AND ! empty($data->technical_datasheet)) {
          unlink('/upload/technicalsss_datasheet/' . $data->technical_datasheet);
      }

      if (file_exists('/upload/testreportsss/' .$data->test_report) AND ! empty($data->test_report)) {
        unlink('/upload/testreportsss/' . $data->test_report);
    }
       $data->delete();
       return redirect()->route('floorings.view')->with('message', 'Product deleted successfully');
}
}

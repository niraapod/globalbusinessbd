<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Product;

class ProductController extends Controller
{
    public function view(){
     
      
        $data['alldata'] = Product::all();
        return view('backend.product.product-view', $data);
}
public function add(){
  return view('backend.product.add-product');
}

public function store(Request $request){
  $data = new Product();
  $data->created_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {

    if ($request->file('image')) {
      $file = $request->file('image');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/product_images'), $filename);
      $data['image'] = $filename;
    }

  }

  if($request->hasFile('technical_datasheet')) {

    if ($request->file('technical_datasheet')) {
      $file = $request->file('technical_datasheet');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/technical_datasheet'), $filename);
      $data['technical_datasheet'] = $filename;
    }
  }

  if($request->hasFile('test_report')) { 

    if ($request->file('test_report')) {
      $file = $request->file('test_report');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/testreport'), $filename);
      $data['test_report'] = $filename;
    }

  }
  
  $data->save();
  return redirect()->route('products.view')->with('message', 'Product add successfully');


}



public function edit($id){
  $editData = Product::findOrFail($id);
  return view ('backend.product.edit-product', compact('editData'));
}

public function update(Request $request, $id){
  $data = Product::findOrFail($id);
  // return $data;
  $data->updated_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) { 
    
    if ($request->file('image')) {
      $file = $request->file('image');
      @unlink(public_path('upload/product_images/'.$data->image));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/product_images'), $filename);
      $data['image'] = $filename;
   
    }

  }
  // else {
  //   echo $data->image;
  // }
  

  if($request->hasFile('technical_datasheet')) { 
    // echo $request->file('technical_datasheet');
    if ($request->file('technical_datasheet')) {
      $file = $request->file('technical_datasheet');
      @unlink(public_path('upload/technical_datasheet/'.$data->technical_datasheet));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/technical_datasheet'), $filename);
      $data['technical_datasheet'] = $filename;
    }

  }
  // else {
  //   echo $data->technical_datasheet;
  // }

  
    if($request->hasFile('test_report')) { 
      // echo $request->file('test_report');
      if ($request->file('test_report')) {
        $file = $request->file('test_report');
        @unlink(public_path('upload/testreport/'.$data->testreport));
        $filename =$file->getClientORiginalName();
        $file->move(public_path('upload/testreport'), $filename);
        $data['test_report'] = $filename;
      }
    }
    // else {
    //   echo $data->test_report;
    // }
    $data->save();
    return redirect()->route('products.view')->with('message', 'Product update successfully');
}
public function delete($id){
       $data = Product::find($id);
       if (file_exists('/upload/product_images/' .$data->image) AND ! empty($data->image)) {
            unlink('/upload/product_images/' . $data->image);
        }

        if (file_exists('/upload/technical_datasheet/' .$data->technical_datasheet) AND ! empty($data->technical_datasheet)) {
          unlink('/upload/technical_datasheet/' . $data->technical_datasheet);
      }

      if (file_exists('/upload/testreport/' .$data->test_report) AND ! empty($data->test_report)) {
        unlink('/upload/testreport/' . $data->test_report);
    }
       $data->delete();
       return redirect()->route('products.view')->with('message', 'Product deleted successfully');
}
}

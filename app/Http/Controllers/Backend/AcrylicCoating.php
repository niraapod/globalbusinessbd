<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Acrylic;

class AcrylicCoating extends Controller
{
    public function view(){
     
      
        $data['alldataaa'] = Acrylic::all();
        return view('backend.productss.productss-view', $data);
}
public function add(){
  return view('backend.productss.add-productss');
}

public function store(Request $request){
  $data = new Acrylic();
  $data->created_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {
  if ($request->file('image')) {
    $file = $request->file('image');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/productss_images'), $filename);
    $data['image'] = $filename;
}
}

if($request->hasFile('technical_datasheet')) {
  if ($request->file('technical_datasheet')) {
    $file = $request->file('technical_datasheet');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/technicalss_datasheet'), $filename);
    $data['technical_datasheet'] = $filename;
}
}

if($request->hasFile('test_report')) {
  if ($request->file('test_report')) {
      $file = $request->file('test_report');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/testreportss'), $filename);
      $data['test_report'] = $filename;
    }
  }
  $data->save();
  return redirect()->route('acryliccoatings.view')->with('message', 'Product add successfully');

}



public function edit($id){
$editData = Acrylic::find($id);
return view ('backend.productss.edit-productss', compact('editData'));
}

public function update(Request $request, $id){
  $data = Acrylic::find($id);
  $data->updated_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {
  if ($request->file('image')) {
      $file = $request->file('image');
      @unlink(public_path('upload/productss_images/'.$data->image));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/productss_images'), $filename);
      $data['image'] = $filename;
    }
  }
    if($request->hasFile('technical_datasheet')) {
      if ($request->file('technical_datasheet')) {
        $file = $request->file('technical_datasheet');
        @unlink(public_path('upload/technicalss_datasheet/'.$data->technical_datasheet));
        $filename =$file->getClientORiginalName();
        $file->move(public_path('upload/technicalss_datasheet'), $filename);
        $data['technical_datasheet'] = $filename;
    }
    }
    
    if($request->hasFile('test_report')) {
      if ($request->file('test_report')) {
          $file = $request->file('test_report');
          @unlink(public_path('upload/testreportss/'.$data->test_report));
          $filename =$file->getClientORiginalName();
          $file->move(public_path('upload/testreportss'), $filename);
          $data['test_report'] = $filename;
        }
      }


      $data->save();
  return redirect()->route('acryliccoatings.view')->with('message', 'Product update successfully');

}
public function delete($id){
       $data = Acrylic::find($id);
       if (file_exists('/upload/productss_images/' .$data->image) AND ! empty($data->image)) {
            unlink('/upload/productss_images/' . $data->image);
        }

        if (file_exists('/upload/technicalss_datasheet/' .$data->technical_datasheet) AND ! empty($data->technical_datasheet)) {
          unlink('/upload/technicalss_datasheet/' . $data->technical_datasheet);
      }

      if (file_exists('/upload/testreportss/' .$data->test_report) AND ! empty($data->test_report)) {
        unlink('/upload/testreportss/' . $data->test_report);
    }
       $data->delete();
       return redirect()->route('acryliccoatings.view')->with('message', 'Product deleted successfully');
}
}

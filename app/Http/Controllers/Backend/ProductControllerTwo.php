<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Models\ProductTwo;


class ProductControllerTwo extends Controller
{
    public function view(){
     
      
        $data['alldataa'] = ProductTwo::all();
        return view('backend.products.products-view', $data);
}
public function add(){
  return view('backend.products.add-products');
}

public function store(Request $request){
  $data = new ProductTwo();
  $data->created_by = Auth::user()->id;
  $data->product_name = $request->product_name;

  if($request->hasFile('image')) {
  if ($request->file('image')) {
    $file = $request->file('image');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/products_images'), $filename);
    $data['image'] = $filename;
}
}

if($request->hasFile('technical_datasheet')) {
  if ($request->file('technical_datasheet')) {
    $file = $request->file('technical_datasheet');
    $filename =$file->getClientORiginalName();
    $file->move(public_path('upload/technicals_datasheet'), $filename);
    $data['technical_datasheet'] = $filename;
}
}

if($request->hasFile('test_report')) {
  if ($request->file('test_report')) {
      $file = $request->file('test_report');
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/testreports'), $filename);
      $data['test_report'] = $filename;
    }
    }
  $data->save();
  return redirect()->route('productss.view')->with('message', 'product add successfully');

}



public function edit($id){
$editData = ProductTwo::find($id);
return view ('backend.products.edit-products', compact('editData'));
}

public function update(Request $request, $id){
  $data = ProductTwo::findOrFail($id);
  $data->updated_by = Auth::user()->id;
  $data->product_name = $request->product_name;
  if($request->hasFile('image')) { 
    if ($request->file('image')) {
      $file = $request->file('image');
      @unlink(public_path('upload/products_images/'.$data->image));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/products_images'), $filename);
      $data['image'] = $filename;
    }
  }
  // else {
  //   echo $data->image;
  // }
  

  if($request->hasFile('technical_datasheet')) { 
    // echo $request->file('technical_datasheet');
    if ($request->file('technical_datasheet')) {
      $file = $request->file('technical_datasheet');
      @unlink(public_path('upload/technicals_datasheet/'.$data->technical_datasheet));
      $filename =$file->getClientORiginalName();
      $file->move(public_path('upload/technicals_datasheet'), $filename);
      $data['technical_datasheet'] = $filename;
    }
  }
  // else {
  //   echo $data->technical_datasheet;
  // }

  
    if($request->hasFile('test_report')) { 
    
      if ($request->file('test_report')) {
        $file = $request->file('test_report');
        @unlink(public_path('upload/testreports'.$data->testreport));
        $filename =$file->getClientORiginalName();
        $file->move(public_path('upload/testreports'), $filename);
        $data['test_report'] = $filename;
      }
    }
  $data->save();
  return redirect()->route('productss.view')->with('message', 'product update successfully');

}
public function delete($id){
       $data = ProductTwo::find($id);
       if (file_exists('/upload/products_images/' .$data->image) AND ! empty($data->image)) {
            unlink('/upload/products_images/' . $data->image);
        }

        if (file_exists('/upload/technicals_datasheet/' .$data->technical_datasheet) AND ! empty($data->technical_datasheet)) {
          unlink('/upload/technicals_datasheet/' . $data->technical_datasheet);
      }

      if (file_exists('/upload/testreports/' .$data->test_report) AND ! empty($data->test_report)) {
        unlink('/upload/testreports/' . $data->test_report);
    }
       $data->delete();
       return redirect()->route('productss.view')->with('message', 'product deleted successfully');
}
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\OurProject;


class OurProjectController extends Controller
{
    public function view(){
     
      
        $data['alldata'] = OurProject::all();
        return view('backend.project.project-view', $data);
}
public function add(){
  return view('backend.project.add-project');
}

public function store(Request $request){
  $data = new OurProject();
  $data->created_by = Auth::user()->id;
  $data->project_name = $request->project_name;

  if($request->hasFile('image_one')) { 
  if ($request->file('image_one')) {
    $file = $request->file('image_one');
    $filename =date('YmdHi').$file->getClientORiginalName();
    $file->move(public_path('upload/project_images'), $filename);
    $data['image_one'] = $filename;
}
}

if($request->hasFile('image_two')) { 
  if ($request->file('image_two')) {
    $file = $request->file('image_two');
    $filename =date('YmdHi').$file->getClientORiginalName();
    $file->move(public_path('upload/image_two'), $filename);
    $data['image_two'] = $filename;
}
}
if($request->hasFile('image_three')) {
  if ($request->file('image_three')) {
      $file = $request->file('image_three');
      $filename =date('YmdHi').$file->getClientORiginalName();
      $file->move(public_path('upload/image_three'), $filename);
      $data['image_three'] = $filename;
    }
  }
  if($request->hasFile('image_four')) {
    if ($request->file('image_four')) {
        $file = $request->file('image_four');
        $filename =date('YmdHi').$file->getClientORiginalName();
        $file->move(public_path('upload/image_four'), $filename);
        $data['image_four'] = $filename;
      }
    }

    if($request->hasFile('image_five')) {
      if ($request->file('image_five')) {
          $file = $request->file('image_five');
          $filename =date('YmdHi').$file->getClientORiginalName();
          $file->move(public_path('upload/image_five'), $filename);
          $data['image_five'] = $filename;
        }
      }


      if($request->hasFile('image_six')) {
        if ($request->file('image_six')) {
            $file = $request->file('image_six');
            $filename =date('YmdHi').$file->getClientORiginalName();
            $file->move(public_path('upload/image_six'), $filename);
            $data['image_six'] = $filename;
          }
        }
  $data->save();
  return redirect()->route('projects.view')->with('message', 'Our Project add successfully');


}



public function edit($id){
$editData = OurProject::find($id);
return view ('backend.project.edit-project', compact('editData'));
}

public function update(Request $request, $id){
  $data = OurProject::find($id);
  $data->updated_by = Auth::user()->id;

  if($request->hasFile('image_one')) { 
  if ($request->file('image_one')) {
      $file = $request->file('image_one');
      @unlink(public_path('upload/project_images/'.$data->image_one));
      $filename =date('YmdHi').$file->getClientORiginalName();
      $file->move(public_path('upload/project_images'), $filename);
      $data['image_one'] = $filename;

  }}

  if($request->hasFile('image_two')) { 
    if ($request->file('image_two')) {
        $file = $request->file('image_two');
        @unlink(public_path('upload/image_two/'.$data->image_two));
        $filename =date('YmdHi').$file->getClientORiginalName();
        $file->move(public_path('upload/image_two'), $filename);
        $data['image_two'] = $filename;
  
    }}

    if($request->hasFile('image_three')) { 
      if ($request->file('image_three')) {
          $file = $request->file('image_three');
          @unlink(public_path('upload/image_three/'.$data->image_three));
          $filename =date('YmdHi').$file->getClientORiginalName();
          $file->move(public_path('upload/image_three'), $filename);
          $data['image_three'] = $filename;
    
      }}

      if($request->hasFile('image_four')) { 
        if ($request->file('image_four')) {
            $file = $request->file('image_four');
            @unlink(public_path('upload/image_four/'.$data->image_four));
            $filename =date('YmdHi').$file->getClientORiginalName();
            $file->move(public_path('upload/image_four'), $filename);
            $data['image_four'] = $filename;
      
        }}

        if($request->hasFile('image_five')) { 
          if ($request->file('image_five')) {
              $file = $request->file('image_five');
              @unlink(public_path('upload/image_five/'.$data->image_four));
              $filename =date('YmdHi').$file->getClientORiginalName();
              $file->move(public_path('upload/image_five'), $filename);
              $data['image_five'] = $filename;
        
          }}

          if($request->hasFile('image_six')) { 
            if ($request->file('image_six')) {
                $file = $request->file('image_six');
                @unlink(public_path('upload/image_six/'.$data->image_four));
                $filename =date('YmdHi').$file->getClientORiginalName();
                $file->move(public_path('upload/image_six'), $filename);
                $data['image_six'] = $filename;
          
            }}
      $data->save();
  return redirect()->route('projects.view')->with('message', 'Our Project update successfully');

}
public function delete($id){
       $data = OurProject::find($id);
       if (file_exists('/upload/project_images_one/' .$data->images_one) AND ! empty($data->images_one)) {
            unlink('/upload/product_images/' . $data->images_one);
        }

        if (file_exists('/upload/image_two/' .$data->image_two) AND ! empty($data->image_two)) {
          unlink('/upload/image_two/' . $data->image_two);
      }

      if (file_exists('/upload/image_three/' .$data->image_three) AND ! empty($data->image_three)) {
        unlink('/upload/image_three/' . $data->image_three);
    }

    if (file_exists('/upload/image_four/' .$data->image_four) AND ! empty($data->image_four)) {
      unlink('/upload/image_four/' . $data->image_four);
  }

  if (file_exists('/upload/image_five/' .$data->image_five) AND ! empty($data->image_five)) {
    unlink('/upload/image_five/' . $data->image_five);
}

if (file_exists('/upload/image_six/' .$data->image_six) AND ! empty($data->image_six)) {
  unlink('/upload/image_six/' . $data->image_six);
}
       $data->delete();
       return redirect()->route('projects.view')->with('message', 'Our Project deleted successfully');
}
}

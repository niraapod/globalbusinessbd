<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Client;

class ClientController extends Controller
{
    public function view(){
        $data['alldata'] = Client::all();
        return view('backend.client.client-view', $data);
}
public function add(){
  return view('backend.client.add-client');
}

public function store(Request $request){
  $data = new Client();
  $data->created_by = Auth::user()->id;
  if ($request->file('image')) {
      $file = $request->file('image');
      $filename =date('YmdHi').$file->getClientORiginalName();
      $file->move(public_path('upload/client_images'), $filename);
      $data['image'] = $filename;
  $data->save();
  return redirect()->route('clients.view')->with('message', 'Client add successfully');
}
}


public function edit($id){
$editData = Client::find($id);
return view ('backend.client.edit-client', compact('editData'));
}

public function update(Request $request, $id){
  $data = Client::find($id);
  $data->updated_by = Auth::user()->id;
  if ($request->file('image')) {
      $file = $request->file('image');
      @unlink(public_path('upload/client_images/'.$data->image));
      $filename =date('YmdHi').$file->getClientORiginalName();
      $file->move(public_path('upload/client_images'), $filename);
      $data['image'] = $filename;
      $data->save();
  return redirect()->route('clients.view')->with('message', 'Client update successfully');
}
}
public function delete($id){
       $data = Client::find($id);
       if (file_exists('/upload/client_images/' .$data->image) AND ! empty($data->image)) {
            unlink('/upload/client_images/' . $data->image);
        }
       $data->delete();
       return redirect()->route('clients.view')->with('message', 'Client deleted successfully');
}
}
